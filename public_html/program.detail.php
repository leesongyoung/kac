<?php include_once "./inc/head.php"; ?>
</head>
<body>
<?php include_once "./inc/header.php"; ?>
	<div id="container" class="edu-program">
		<div id="top">
			<p class="title">교육프로그램</p>
		</div>
		<div id="article">
			<div class="header bxsdw">
				<h3 class="title">교육프로그램</h3>
				<div class="location">
					<ul>
						<li class="home"><a href="/" >Home</a></li>
						<li class="current"><span>교육프로그램</span></li>
					</ul>
				</div>
			</div>
			<div class="section view bxsdw">
				<table class="view-infos">
					<colgroup>
						<col width="110px" />
						<col width="*" />
						<col width="110px" />
						<col width="*" />
					</colgroup>
					<tbody>
						<tr>
							<th class="subject">교육명</th>
							<td class="subject" colspan="3"><strong>0000 교육프로그램 제목</strong></td>
						</tr>
						<tr>
							<th>교육일시</th>
							<td><span>2017.12.12 09:00 ~ 14:00</span></td>
							<th>접수기간</th>
							<td><span>2017.10.15~12.02</span></td>
						</tr>
						<tr>
							<th>교육차수</th>
							<td><span>2차</span></td>
							<th>교육장소</th>
							<td><span>00유스호스텔</span></td>
						</tr>
						<!-- 
						<tr>
							<th>목록 썸네일</th>
							<td class="file" colspan="3"><a href="#">XXXX.jpg</a></td>
						</tr>
						-->
					</tbody>
				</table>
				<div class="view-contents">
					<p>ㅇㅇ교육입니다. <br />
					참가신청해주세요. <br /><br />
					</p>
					<img src="/public/images/sample.jpg" alt="" />
				</div>
				<div class="btn-group">
					<ul class="controls-util">
						<li><a href="#" class="btn"><span>Prev</span></a></li>
						<li><a href="#" class="btn"><span>Next</span></a></li>
					</ul>
					<ul class="local-util">
						<li><a href="/program.list.php" class="btn"><span>List</span></a></li>
					</ul>
				</div>
			</div>
			<form action="?" class="search-section">
				<select name="" id="" class="condition">
					<option value="">제목 + 내용</option>
				</select>
				<input type="search" class="i-search" />
				<button type="submit" class="btn-submit"><span>검색</span></button>
			</form>
		</div>
	</div>
<?php include_once "./inc/footer.php"; ?>
</body>
</html>