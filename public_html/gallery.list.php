<?php 
include_once "./_init.php";
include_once "./inc/head.php"; 
$index_page = "gallery.list.php";
$query_page = "query.php";
if(!$jb_code) $jb_code="30";
?>
</head>
<body>
<?php include_once "./inc/header.php"; ?>
	<div id="container" class="gallery">
		<div id="top">
			<p class="title">활동사진</p>
		</div>
		<div id="article">
			<div class="header bxsdw">
				<h3 class="title">활동사진</h3>
				<div class="location">
					<ul>
						<li class="home"><a href="/" >Home</a></li>
						<li class="current"><span>활동사진</span></li>
					</ul>
				</div>
			</div>
			<?php include $GP -> INC_PATH ."/board_inc.php"; ?>
		</div>
	</div>
<?php include_once "./inc/footer.php"; ?>
</body>
</html>