<?php 
include_once "./_init.php";
include_once "./inc/head.php"; 
$index_page = "lesson-file.php";
$query_page = "query.php";
if(!$jb_code) $jb_code="40";
?>
</head>
<body>
<?php include_once "./inc/header.php"; ?>
	<div id="container" class="lesson-file">
		<div id="top">
			<p class="title">강의교안/교재</p>
		</div>
		<div id="article">
			<div class="header bxsdw">
				<h3 class="title">강의교안/교재</h3>
				<div class="location">
					<ul>
						<li class="home"><a href="/" >Home</a></li>
						<li class="current"><span>강의교안/교재</span></li>
					</ul>
				</div>
			</div>
			<div class="section">
				<ul class="lessonfile-list bxsdw">
					<li><div class="panel">
						<div class="picture"><img src="/public/images/book-sample.jpg" alt="" class="block" /></div>
						<dl class="info">
							<dt>NCS 강의교안</dt>
							<dd class="publish">한국능력개발인증원</dd>
							<dd class="date">2017년 1월</dd>
						</dl>
						<a href="#" class="btn-download"><span>다운로드</span></a>
					</div></li>
					<li><div class="panel">
						<div class="picture"><img src="/public/images/book-sample.jpg" alt="" class="block" /></div>
						<dl class="info">
							<dt>NCS 강의교안</dt>
							<dd class="publish">한국능력개발인증원</dd>
							<dd class="date">2017년 1월</dd>
						</dl>
						<a href="#" class="btn-download"><span>다운로드</span></a>
					</div></li>
					<li><div class="panel">
						<div class="picture"><img src="/public/images/book-sample.jpg" alt="" class="block" /></div>
						<dl class="info">
							<dt>NCS 강의교안</dt>
							<dd class="publish">한국능력개발인증원</dd>
							<dd class="date">2017년 1월</dd>
						</dl>
						<a href="#" class="btn-download"><span>다운로드</span></a>
					</div></li>
				</ul>
				<div class="pagination">
					<a href="#" class="btn controls prev">
						<i class="ip-icon-page-prev"></i>
						<span class="text-ir">이전페이지</span>
					</a>
					<strong class="btn current">1</strong>
					<a href="#" class="btn">2</a>
					<a href="#" class="btn">3</a>
					<a href="#" class="btn">4</a>
					<a href="#" class="btn">5</a>
					<a href="#" class="btn controls next">
						<i class="ip-icon-page-next"></i>
						<span class="text-ir">다음페이지</span>
					</a>
				</div>
			</div>
			<form action="?" class="search-section">
				<select name="" id="" class="condition">
					<option value="">제목 + 내용</option>
				</select>
				<input type="search" class="i-search" />
				<button type="submit" class="btn-submit"><span>검색</span></button>
			</form>
		</div>
	</div>
<?php include_once "./inc/footer.php"; ?>
</body>
</html>