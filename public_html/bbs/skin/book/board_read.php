<div class="section view bxsdw">
        <table class="view-infos">
            <colgroup>
                <col width="110px" />
                <col width="*" />
                <col width="110px" />
                <col width="*" />
            </colgroup>
            <tbody>
                <tr>
                    <th class="subject">제목</th>
                    <td class="subject" colspan="3"><strong><?=$jb_title?></strong></td>
                </tr>
                <tr>
                    <th>작성일</th>
                    <td><span><?=$jb_reg_date?></span></td>
                    <th>작성자</th>
                    <td><span><?=$jb_name?></span></td>
                </tr>
                <tr>
                    <th>첨부파일</th>
                    <td class="file" colspan="3">
						<?php
                        if($file_cnt > 0)
                        {
                            for($i=0; $i<$file_cnt; $i++)
                            {
                                if($ex_jb_file_name[$i])
                                {		
                                    $code_file = $GP->UP_IMG_SMARTEDITOR. "jb_${jb_code}/${ex_jb_file_code[$i]}";							
                                    echo "<a href=\"/bbs/download.php?downview=1&file=" . $code_file . "&name=" . $ex_jb_file_name[$i] . " \">$ex_jb_file_name[$i]</a>";							
                                    if($i < ($file_cnt-1))
                                        echo ", ";
                                }	 
                            }
                        } 
                        ?>
                    </td>
                </tr>
            </tbody>
        </table>
        <div class="view-contents">
		   <?php
                    if($file_cnt > 0) {
                        for($i=0; $i<$file_cnt; $i++)	{
                            if($ex_jb_file_name[$i]) {
                                //파일의 확장자
                                $file_ext = substr( strrchr($ex_jb_file_name[$i], "."),1); 
                                $file_ext = strtolower($file_ext);	//확장자를 소문자로...
                                
                                if ($file_ext=="gif" || $file_ext=="jpg" || $file_ext=="png" || $file_ext=="bmp") {										
                                    echo "<a href='" . $GP->UP_IMG_SMARTEDITOR_URL ."jb_${jb_code}/${ex_jb_file_code[$i]}' target='_blank'>";
                                    echo "<img src=\"" . $GP->UP_IMG_SMARTEDITOR_URL ."jb_" . $jb_code . "/" . $ex_jb_file_code[$i] ."\" class='imgResponsive'>";
                                    echo "</a>";
                                }
                            }	 
                        }
                    }
                ?>  
                <br>
            <?=$content?>
        </div>
        <div class="btn-group">
            <ul class="controls-util">
                <li><a href="<? if($get_par1) echo $get_par1; else "javascript:void(0)";?>" class="btn"><span>Prev</span></a></li>
                <li><a href="<? if($get_par2) echo $get_par2; else "javascript:void(0)";?>" class="btn"><span>Next</span></a></li>
            </ul>
            <ul class="local-util">
                <li><a href="<?=$index_page?>?jb_code=<?=$jb_code?>&<?=$search_key?>&search_keyword=<?=$search_keyword?>&page=<?=$page?>" class="btn"><span>List</span></a></li>
            </ul>
        </div>
    </div>
