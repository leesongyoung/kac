<? if($jb_code != 20){ 
		$tit = "제목";
		$size = "(390 * 390)";
	}else{
		$tit = "교육명";
		$size = "(570 * 390)";
	}
?>
<script type="text/javascript" src="<?=$GP -> JS_SMART_PATH?>/HuskyEZCreator.js" charset="utf-8"></script>
<script type="text/javascript" src="<?=$GP -> JS_PATH?>/admin/jquery.base64.js"></script>
<?php $r5_select = $C_Func -> makeSelect('jb_type', $GP->R5_TYPE, $jb_type, '','선택하세요');
	   $r5_cate = $C_Func -> makeSelect('jb_etc2', $GP->R5_TYPE2, $jb_etc2, '','선택하세요');
?>
 <div class="Wrap_layer"><!--// 전체를 감싸는 Wrap -->
	<div class="boxContent_layer">

<form name="frm_Board" id="frm_Board" action="<?=$get_par;?>" method="post" enctype="multipart/form-data">
  <input type="hidden" id="jb_name" name="jb_name" value="<?=$check_name?>" />
  <input type="hidden" id="jb_email" name="jb_email" value="<?=$_SESSION['suseremail']?>" />
  <input type="hidden" id="before_front_file" name="before_front_file" value="<?=$jb_front_image?>" />
  <input type="hidden" name="jb_password" value="<?=$input_passd;?>">
    		
    <div class="boxContentBody">
        <div class="boxMemberInfoTable_layer">				
            <table class="table table-bordered">
        <colgroup>
        <col style="width:15%" />
        <col />
      </colgroup>
		<tbody>	
             <tr class="row">
                <th scope="row"><span class="star">*</span><?=$tit?></th>
                <td >
                    <input type="text" class="input_text" title="<?=$tit?> 입력" placeholder="<?=$tit?>을 입력해 주세요." autocomplete="off" id="jb_title" name="jb_title" style="width:80%;" value="<?=$jb_title;?>" />
                </td>
            </tr>
			<? if($check_level >= 2){?>
             <tr>
                <th>노출여부</th>
                <td>
                    <select name="jb_show" id="jb_show">
					  <option value="B" <? if($jb_show == "B") { echo "selected"; }?>>게시</option>
					  <option value="A" <? if($jb_show == "A") { echo "selected"; }?>>비게시</option>
					</select>
                </td>
            </tr>	
			<? } ?>
			<? if($jb_code == 20){ ?>
				<tr>
					<th scope="row"><span class="star">*</span>교육일시</th>
					<td >
						<input type="text" class="input_text" title="교육일시 입력" placeholder="교육일시를 입력해 주세요." autocomplete="off" value="<?=$jb_edu_time;?>" id="jb_edu_time" name="jb_edu_time" style="width:35%;" />
					</td>
				</tr>
				<tr>
					<th scope="row"><span class="star">*</span>접수기간</th>
					<td >
						<input type="text" class="input_text" title="접수기간 입력" placeholder="접수기간을 입력해 주세요." autocomplete="off" value="<?=$jb_edu_day;?>" id="jb_edu_day" name="jb_edu_day" style="width:35%;" />
					</td>
				</tr>
				<tr>
					<th scope="row"><span class="star">*</span>교육차수</th>
					<td >
						<input type="text" class="input_text" title="교육차수 입력" placeholder="교육차수를 입력해 주세요." autocomplete="off" value="<?=$jb_edu_num;?>" id="jb_edu_num" name="jb_edu_num" style="width:35%;" />
					</td>
				</tr>
				<tr>
					<th scope="row"><span class="star">*</span>교육장소</th>
					<td >
						<input type="text" class="input_text" title="교육장소 입력" placeholder="교육장소를 입력해 주세요." autocomplete="off" value="<?=$jb_edu_place;?>" id="jb_edu_place" name="jb_edu_place" style="width:35%;" />
					</td>
				</tr>
			<? } ?>
			<? if($jb_code == 30){ ?>
				<tr>
					<th scope="row"><span class="star">*</span>프로그램명</th>
					<td >
						<input type="text" class="input_text" title="프로그램명 입력" placeholder="프로그램명을 입력해 주세요." autocomplete="off" id="jb_program" name="jb_program" style="width:35%;" value="<?=$jb_program;?>"  />
					</td>
				</tr>
			<? } ?>
			<? if($jb_code == 40){ ?>
				<tr>
					<th scope="row"><span class="star">*</span>컨텐츠 저자</th>
					<td >
						<input type="text" class="input_text" title="컨텐츠 저자 입력" placeholder="컨텐츠 저자를 입력해 주세요." autocomplete="off" id="jb_etc2" name="jb_etc2" style="width:15%;" value="<?=$jb_etc2;?>" />
					</td>
				</tr>
				<tr>
					<th scope="row"><span class="star">*</span>컨텐츠 제작일</th>
					<td >
						<input type="text" class="input_text datepicker" title="컨텐츠 제작일 입력" placeholder="컨텐츠 제작일을 입력해 주세요." autocomplete="off" id="jb_etc3" name="jb_etc3" style="width:15%;" value="<?=$jb_etc3;?>" />
					</td>
				</tr>
			 <? } ?>
			 <tr>
              <th scope="row">설정</th>
              <td>
			<!--	<?
				//공지는 관리자만 할 수 있다.
				if(isset($check_id) && $check_level >= 2) {
				  if($jb_order=="50")
					$notice_checked=" checked";
				  else
					$notice_checked="";											
				  echo "<label class='noti'><input type=\"checkbox\" name=\"jb_notice_check\" value=\"Y\" class='chk' ${notice_checked}>BEST설정</label>";
				}
				?>
		-->
                <?
				if(isset($check_id) && $check_level >= 2) {
				  if($jb_main_check=="Y")
					$jb_main_check=" checked";
				  else
					$jb_main_check="";											
				  echo "<label class='noti'><input type=\"checkbox\" name=\"jb_main_check\" value=\"Y\" class='chk' ${jb_main_check}>메인글설정</label>";
				}
				?>		
              </td>
            </tr>
            <tr>
              <th scope="row">대표이미지</th>
              <td class="files">
               <div class="inputFile">
                    <label class="fileBtn">
                        <input type="file" title="파일선택" name="jb_front_image" id="jb_front_image" /><?=$size?>
                    </label>
                  </div> 
				   <?
						if($jb_front_image != "") {
							echo  $jb_front_image;
							echo "<label> <input type=\"checkbox\" name=\"del_file_front\" value=\"Y\"> X </label>";			
						} 
					?>
              </td>
            </tr> 
                       
            <tr>
                <th scope="row">첨부파일</th>
                <td class="files">
                <input type="file" name="jb_file[]" id="jb_file" size="30">
                <?
						if($jb_file_code != "") {
							echo  $jb_file_code;
							echo "<label> <input type=\"checkbox\" name=\"del_file\" value=\"Y\"> X </label>";			
						} 
					?>
				<!--?php
                  if($ex_jb_file_name[$i]){
                      $jb_file_code = $GP->UP_IMG_SMARTEDITOR. "/jb_${jb_code}/${ex_jb_file_code[$i]}";							
                      echo "<a href=\"/bbs/download.php?downview=1&file=" . $jb_file_code . "&name=" . $ex_jb_file_name[$i] . " \">$ex_jb_file_name[$i]</a>";
                      echo " <input type=\"checkbox\" name=\"del_file[${i}]\" value=\"Y\"> X";				
                  }			
                ?-->	
                 
                </td>
            </tr>                   
			<tr>
              <th scope="row">본문</th>
              <td>
                <textarea name="jb_content" id="jb_content" style="display:none"></textarea>
                <textarea name="ir1" id="ir1" style="width:100%; height:300px; min-width:280px; display:none;"><?=$jb_content;?></textarea>
              </td>
	        </tr>   
             <? if($check_level < 2) {?>
            <tr>
              <th scope="row">자동입력방지</th>
              <td>
                <strong class="mobTh">자동입력방지</strong>
                <img src="<?=$GP -> IMG_PATH?>/zmSpamFree/zmSpamFree.php?zsfimg=<?php echo time();?>" id="zsfImg" alt="아래 새로고침을 클릭해 주세요." style="vertical-align:middle;" />
                <input type="text" class="i_text" title="자동입력방지 숫자 입력" style="width:60px;" name="zsfCode" id="zsfCode" />
                <a href="#;" class="btnT btnReplace" onclick="document.getElementById('zsfImg').src='<?=$GP -> IMG_PATH?>/zmSpamFree/zmSpamFree.php?re&zsfimg=' + new Date().getTime(); return false;">새로고침</a>
              </td>
            </tr>
            <? } ?>
		</tbody>
    </table>
	<div style="margin-top:5px; text-align:center;">
       <button type="button" id="img_submit" class="btnSearch ">글수정</button>
       <button type="button" id="img_cancel" class="btnSearch">취소</button>
  	 </div>
</div>
</div>
  <input type="hidden" name="img_full_name" id="img_full_name" value="<?=$jb_img_code;?>" />
  <input type="hidden" name="upfolder" id="upfolder" value="jb_<?=$jb_code?>" />
</form>
</div>


<script type="text/javascript">

	$(function(){
		var datePicker = $('.datepicker');
		if(datePicker.val() == '') datePicker.val(new Date().format('yyyy-MM-dd'));
		console.log(datePicker.val())
		datePicker.datepicker({
			monthNames: ['1','2','3','4','5','6','7','8','9','10','11','12'],
			monthNamesShort: ['1','2','3','4','5','6','7','8','9','10','11','12'],
			dayNamesMin: [ "sun", "mon", "tue", "wed", "thu", "fri", "sat" ],
			closeText: '닫기',
			prevText: '이전달',
			nextText: '다음달',
			currentText: '오늘',
			dateFormat: 'yy-mm-dd',
			showMonthAfterYear: true,
			changeMonth: true,
			changeYear: true,
			yearRange: "-120:+0",
			onDraw:function(){
				$('.ui-datepicker-year').after('<span>년</span>');
				$('.ui-datepicker-month').after('<span>월</span>');
			}
		});
	});
	
	var oEditors = [];
	nhn.husky.EZCreator.createInIFrame({
		oAppRef: oEditors,
		elPlaceHolder: "ir1",
		sSkinURI: "/bbs/smarteditor/SmartEditor2Skin.html",
		htParams : {
			bUseToolbar : true,				// 툴바 사용 여부 (true:사용/ false:사용하지 않음)
			bUseVerticalResizer : true,		// 입력창 크기 조절바 사용 여부 (true:사용/ false:사용하지 않음)
			bUseModeChanger : true,			// 모드 탭(Editor | HTML | TEXT) 사용 여부 (true:사용/ false:사용하지 않음)
			//aAdditionalFontList : aAdditionalFontSet,		// 추가 글꼴 목록
			fOnBeforeUnload : function(){
				//alert("완료!");
			}
		}, //boolean
		fOnAppLoad : function(){
			//예제 코드
			//oEditors.getById["ir1"].exec("PASTE_HTML", ["로딩이 완료된 후에 본문에 삽입되는 text입니다."]);
		},
		fCreator: "createSEditor2"
	});
	
/*	
	$('#img_cancel').click(function(){
		if($_SESSION['suserlevel'] >= "2" && $_SESSION['suserlevel'] <= "4"){
			location.href = '?jb_code=<?=$jb_code?>&<?=$search_key?>&search_keyword=<?=$search_keyword?>&page=<?=$page?>&admin_page=Y&tbs_branch=<?=$tbs_branch?>';
		}else{
			location.href = '?jb_code=<?=$jb_code?>&<?=$search_key?>&search_keyword=<?=$search_keyword?>&page=<?=$page?>&admin_page=Y';
		}
	});
*/

	$('#img_cancel').click(function(){	
		history.go(-1);
	});

	
	$('#img_submit').click(function(){
		
		if($('#jb_title').val() == '')	{
			alert('제목을 입력하세요');
			$('#jb_title').focus();
			return false;
		}		
		
/*		if($('#jb_name').val() == '')	{
			alert('이름을 입력하세요');
			$('#jb_name').focus();
			return false;
		}
*/		
		if($('#jb_password').val() == '')	{
			alert('비밀번호를 입력하세요');
			$('#jb_password').focus();
			return false;
		}
		
/*		if($('#jb_email').val() == '' || !CheckEmail($('#jb_email').val()))	{
			alert('이메일을 정확히 입력하세요');
			$('#jb_email').focus();
			return false;
		}
*/		
		<? if($check_level < 9) {?>
			if($('#zsfCode').val() == '')	{
				alert('자동방지 입력키를 입력하세요');
				$('#zsfCode').focus();
				return false;
			}		
		<? } ?>
		
		oEditors.getById["ir1"].exec("UPDATE_CONTENTS_FIELD", []);
	
		var con	= $('#ir1').val();
		$('#jb_content').val(con);		

		if($('#jb_content').val() == '')
		{
			alert('내용을 입력하세요');
			return false;
		}	
		var t = $.base64Encode($('#ir1').val());		
		$('#jb_content').val(t);

		$('#frm_Board').submit();
		return false;
		
	});

	function CheckEmail(str)
	{
	   var filter=/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i
	   if (filter.test(str)) { return true; }
	   else { return false; }
	}	
	
	function insertIMG(filename){
		var tname = document.getElementById('img_full_name').value;

		if(tname != "")
		{
			document.getElementById('img_full_name').value = tname + "," + filename;
		}
		else
		{
			document.getElementById('img_full_name').value = filename;
		}
	}

</script>
