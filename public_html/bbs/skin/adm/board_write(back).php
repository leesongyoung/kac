<script type="text/javascript" src="<?=$GP -> JS_SMART_PATH?>/HuskyEZCreator.js" charset="utf-8"></script>
<script type="text/javascript" src="<?=$GP -> JS_PATH?>/admin/jquery.base64.js"></script>
 <div class="Wrap_layer"><!--// 전체를 감싸는 Wrap -->
	<div class="boxContent_layer">

<form name="frm_Board" id="frm_Board" action="<?=$get_par;?>" method="post" enctype="multipart/form-data">
  <input type="hidden" id="jb_name" name="jb_name" value="<?=$check_name?>" />
  <input type="hidden" id="jb_email" name="jb_email" value="<?=$_SESSION['suseremail']?>" />
    		
    <div class="boxContentBody">
        <div class="boxMemberInfoTable_layer">				
            <table class="table table-bordered">
        <colgroup>
        <col style="width:15%" />
        <col />
      </colgroup>
		<tbody>	
       
        <tr>
          <th scope="row">작성일자</th>
          <td><input type="text" class="input_text datepicker" title="작성일자 입력" placeholder="작성일자를 입력해 주세요."id="jb_etc1" name="jb_etc1" /></td>
        </tr> 
      <tr>
             <tr class="row">
                <th scope="row"><span class="star">*</span>제목</th>
                <td >
                    <input type="text" class="input_text" title="제목 입력" placeholder="제목을 입력해 주세요." autocomplete="off" id="jb_title" name="jb_title" style="width:80%;" />
                </td>
            </tr>
           
             <tr>
              <th scope="row">태그</th>
               <td><input type="text" class="input_text" title="태그"  autocomplete="off" id="jb_tag" name="jb_tag" style="width:80%;"  /></td>
            </tr> 
			
             <tr>
                <th>노출여부</th>
                <td>
                    <input type="radio" name="jb_show" value="B"  />공개
                    <input type="radio" name="jb_show" value="A"  />비공개
                </td>
            </tr>	
			
            
			<?php
				//회원일 경우 비밀번호를 입력할 필요가 없다.
				if(empty($check_id)) {
				?>
				<tr>
				  <th scope="row">비밀번호</th>
				  <td><input type="text" class="txt w100" title="비밀번호 입력" placeholder="비밀번호를 입력해 주세요." id="jb_password" name="jb_password" /></td>
				</tr>
				<?php
				} else {
				  $password_key=md5($check_id);	
				  $tm=explode(" ",microtime());
				  $jb_password=$password_key . $tm[1];
				  echo ("<input type=\"hidden\" name=\"jb_password\" value=\"${jb_password}\">");
				}
				?>
			 <tr>
              <th scope="row">공지글설정</th>
              <td>
                <?
                if(isset($check_id) && $check_level>=2){
                  echo "<label class='noti'><input type=\"checkbox\" value=\"Y\" id=\"jb_notice_check\" name=\"jb_notice_check\" class='chk'> 적용</label>";
                }
                ?>
              </td>
            </tr>
       
            <tr>
              <th scope="row">대표썸네일</th>
              <td class="files">
               <div class="inputFile">
                    <label class="fileBtn">
                        <input type="file" title="파일선택" name="jb_front_image" id="jb_front_image" />(600 * 400)
                    </label>
                  </div>            
              </td>
            </tr> 
      	                      
            <tr>
                <th scope="row">타이틀이미지</th>
                <td class="files">
                	<input type="file" name="jb_file[]" id="jb_file" size="30">(1400 * 440)
                </td>
            </tr> 
        
              <th scope="row">본문</th>
              <td>
                <textarea name="jb_content" id="jb_content" style="display:none"></textarea>
                <textarea name="ir1" id="ir1" style="width:80%; height:300px; min-width:280px; display:none;"></textarea>
              </td>
	        </tr> 
       
             <? if($check_level < 2) {?>
            <tr>
              <th scope="row">자동입력방지</th>
              <td>
                <strong class="mobTh">자동입력방지</strong>
                <img src="<?=$GP -> IMG_PATH?>/zmSpamFree/zmSpamFree.php?zsfimg=<?php echo time();?>" id="zsfImg" alt="아래 새로고침을 클릭해 주세요." style="vertical-align:middle;" />
                <input type="text" class="i_text" title="자동입력방지 숫자 입력" style="width:60px;" name="zsfCode" id="zsfCode" />
                <a href="#;" class="btnT btnReplace" onclick="document.getElementById('zsfImg').src='<?=$GP -> IMG_PATH?>/zmSpamFree/zmSpamFree.php?re&zsfimg=' + new Date().getTime(); return false;">새로고침</a>
              </td>
            </tr>
            <? } ?>
		</tbody>
    </table>
    <div style="margin-top:5px; text-align:center;">
        <button id="img_submit" class="btnSearch ">등록</button>
        <button id="img_cancel" class="btnSearch ">취소</button>
    </div>
</div>
</div>
  <input type="hidden" name="jb_bruse_check" value="Y" checked>
  <input type="hidden" name="img_full_name" id="img_full_name" />
  <input type="hidden" name="upfolder" id="upfolder" value="jb_<?=$jb_code?>" />
</form>

<script type="text/javascript">
/*
	fn.init = function(){
		fn.datePicker = $(".datepicker");
		if (fn.datePicker.val() == '') fn.datePicker.val(new Date().format('yyyy-MM-dd'));
		fn.datePicker.datepicker({
			monthNames: ['1','2','3','4','5','6','7','8','9','10','11','12'],
			monthNamesShort: ['1','2','3','4','5','6','7','8','9','10','11','12'],
			dayNamesMin: [ "sun", "mon", "tue", "wed", "thu", "fri", "sat" ],
			closeText: '닫기',
			prevText: '이전달',
			nextText: '다음달',
			currentText: '오늘',
			dateFormat: 'yy-mm-dd',
			showMonthAfterYear: true,
			changeMonth: true,
			changeYear: true,
			yearRange: "-120:+0",
			onDraw:function(){
				$('.ui-datepicker-year').after('<span>년</span>');
				$('.ui-datepicker-month').after('<span>월</span>');
			}
		});
	};
*/
	var oEditors = [];
	nhn.husky.EZCreator.createInIFrame({
		oAppRef: oEditors,
		elPlaceHolder: "ir1",
		sSkinURI: "/bbs/smarteditor/SmartEditor2Skin.html",
		htParams : {
			bUseToolbar : true,				// 툴바 사용 여부 (true:사용/ false:사용하지 않음)
			bUseVerticalResizer : true,		// 입력창 크기 조절바 사용 여부 (true:사용/ false:사용하지 않음)
			bUseModeChanger : true,			// 모드 탭(Editor | HTML | TEXT) 사용 여부 (true:사용/ false:사용하지 않음)
			//aAdditionalFontList : aAdditionalFontSet,		// 추가 글꼴 목록
			fOnBeforeUnload : function(){
				//alert("완료!");
			}
		}, //boolean
		fOnAppLoad : function(){
			//예제 코드
			//oEditors.getById["ir1"].exec("PASTE_HTML", ["로딩이 완료된 후에 본문에 삽입되는 text입니다."]);
		},
		fCreator: "createSEditor2"
	});
	
	
	$('#img_cancel').click(function(){	
		history.back(-1);
	});

	
	$('#img_submit').click(function(){
		
		if($('#jb_title').val() == '')	{
			alert('제목을 입력하세요');
			$('#jb_title').focus();
			return false;
		}		
		
/*		if($('#jb_name').val() == '')	{
			alert('이름을 입력하세요');
			$('#jb_name').focus();
			return false;
		}
*/		

		if(!$('input:radio[name=jb_show]:checked').val()) {
			alert("게시여부를 선택해주세요.");
			$('#jb_show').focus();
			return false;
		}

		if($('#jb_password').val() == '')	{
			alert('비밀번호를 입력하세요');
			$('#jb_password').focus();
			return false;
		}
		
/*		if($('#jb_email').val() == '' || !CheckEmail($('#jb_email').val()))	{
			alert('이메일을 정확히 입력하세요');
			$('#jb_email').focus();
			return false;
		}
*/	
		oEditors.getById["ir1"].exec("UPDATE_CONTENTS_FIELD", []);
	
		var con	= $('#ir1').val();
		
		
		$('#jb_content').val(con);		

		if($('#jb_content').val() == '' || $('#jb_content').val() == '<br> ')
		{
			alert('내용을 입력하세요');
			return false;
		}	
		var t = $.base64Encode($('#ir1').val());		
		$('#jb_content').val(t);
		
			
		<? if($check_level < 9) {?>	
			if($('#zsfCode').val() == '')	{
				alert('자동방지 입력키를 입력하세요');
				$('#zsfCode').focus();
				return false;
			}		
		<? } ?>

		$('#frm_Board').submit();
		return false;
		
	});
	

	function CheckEmail(str)
	{
		 var filter=/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i
		 if (filter.test(str)) { return true; }
		 else { return false; }
	}

	function insertIMG(filename){
		var tname = document.getElementById('img_full_name').value;

		if(tname != "")
		{
			document.getElementById('img_full_name').value = tname + "," + filename;
		}
		else
		{
			document.getElementById('img_full_name').value = filename;
		}
	}
</script>
