<?
	include_once  '../../_init.php';
	include_once($GP -> CLS."class.jhboard.php");
	$C_JHBoard = new JHBoard();

	$cp_page = $_POST["cp_page"];

	$l_max = 6+($cp_page*5);
	$l_min = $l_max-5;
	global $GP, $C_JHBoard, $C_Func;

	$args = array();
	$args['jb_code'] = "10";
	$args['order'] = "order by A.jb_order ASC, A.jb_depth DESC";
	$args['limit']  = " limit $l_min,5";
	$args['main_show2'] = "B";  //게시/비게시
	$rst = $C_JHBoard->Board_Main_Data($args);
	$GP -> MEMBER_CONFIG_LEVEL[$mb_level];
	$args['mb_level'] = "5"; 
	
	$str = "";
	for($i=0; $i<count($rst); $i++) {
		$jb_idx				= $rst[$i]['jb_idx'];
		$jb_code			= $rst[$i]['jb_code'];
		$jb_file_code		= $rst[$i]['jb_file_code'];
		$jb_file_name		= $rst[$i]['jb_file_name'];
		$jb_front_image 	= $rst[$i]['jb_front_image'];	
		$jb_type2 			= $rst[$i]['jb_type2'];
		$jb_tag 			= $rst[$i]['jb_tag'];
		$jb_title 			= $C_Func->strcut_utf8($rst[$i]['jb_title'], 60, true, "...");
		$jb_content			= $C_Func->dec_contents_edit($rst[$i]['jb_content']);
		$jb_content			= trim(strip_tags($jb_content));
		$jb_content 		= $C_Func->strcut_utf8($jb_content, 100, true, "...");	//제목 (길이, HTML TAG제한여부 처리)
		$jb_reg_date 		= date("Y.m.d", strtotime($rst[$i]['jb_reg_date']));		
		$jb_comment_count	= $C_Func->num_f($rst[$i]['jb_comment_count']);
		
		$url = "";
		if($jb_code == "10") { 
			$url = "/medical.html?jb_code=" . $jb_code . "&jb_idx=" . $jb_idx."&jb_etc2=" . $jb_etc2; 
		}	

		$img_src = '';
		if($jb_front_image != '') {
			$code_file = $GP->UP_IMG_SMARTEDITOR_URL. "/jb_${jb_code}/${jb_front_image}";
			$img_src = "<img src='" . $code_file. "' alt='" . $jb_title_ori . "'  class='block' >";	
		}else{			
			$img_src = "<img src='/public/images/news-sample05.jpg' alt='이미지 없음' class='block' >";	
		}
		
		$tag_txt = "";
		$tag_array = explode(",",$jb_tag);
		foreach($tag_array as $val) {
			$tag_txt .= "<span>#".$val."</span>";
		}

		$str .= "
					<li>
						<a href='" . $get_par . "'>
							<div class='sub'>
								<div class='picture'>" . $img_src ."</div>
								<h4 class='tit'>${jb_title}</h4>
								<div class='descript'>
									<p class='text'>${jb_content}</p>
									<p class='tag'>
										".$tag_txt."
									</p>
									<dl>
										<dt class='none'>날짜</dt>
										<dd class='date'><i class='ip-icon-date'></i>${jb_reg_date}</dd>
										<dt class='none'>작성자</dt>
										<dd class='name'>알오컨텐츠</dd>
									</dl>
								</div>
							</div>
						</a>
					</li>
		";
	}
	
	echo $str;
?>