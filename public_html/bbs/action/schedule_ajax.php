<?php
//error_reporting(E_ALL^E_NOTICE);
//ini_set("display_errors", 1);

	include_once $_SERVER['DOCUMENT_ROOT'] . '/_init.php';
    include_once($GP->CLS."class.list.php");
   	include_once($GP -> CLS."/class.course.php");
	$C_Course 	= new Course;	
	$C_ListClass 	= new ListClass;

	$args = array();
	$cp_page =  $_POST["cp_page"];
	$args['cp_page'] = $_POST["cp_page"];
	$args["co_mem_id"]    = $_POST["co_mem_id"];
	$args["co_idx"]		  = $_POST["co_idx"];

	$data = "";	
	$data = $C_Course->Course_New_List(array_merge($_GET,$_POST,$args));
	$data_list 			= $data;
	$data_list_cnt 	= count($data) -1;
	$total_cnt = $data["t_cnt"];
	$totalcount_l = $total_cnt;
	$k = $totalcount_l - ($cp_page *5);

?>
	<?
			$dummy = 1;
			for ($i = 0 ; $i < $data_list_cnt ; $i++) {
				$co_idx 		= $data_list[$i]['co_idx'];
				$co_title		= $data_list[$i]['co_title'];
				$co_satis		= $data_list[$i]['co_satis'];
				$co_field		= $data_list[$i]['co_field'];
				$co_mem_id		= $data_list[$i]['co_mem_id'];
				$co_mem_name	= $data_list[$i]['co_mem_name'];
				$co_start_date	= date("Y.m.d H:i:s", strtotime($data_list[$i]['co_start_date']));
				$co_end_date	= date("Y.m.d H:i:s", strtotime($data_list[$i]['co_end_date']));
				//$co_satis = ($co_satis) ? $co_satis : "-";
				
				$nDate = date("Y-m-d H:i:s");
				$eDate = date("Y-m-d H:i:s", strtotime($data_list[$i]['co_start_date']));
				
				$datetime1 = date_create($nDate);
				$datetime2 = date_create($eDate);
				$interval = date_diff($datetime1, $datetime2);
				$new_icon = ($nDate < $eDate && $interval->days < 2) ? '<span class="next">NEXT</span>': "";
				//echo $nDate."//".$eDate;
				//$interval = date_diff($nDate, $eDate);
				$data = $C_Course->Course_Satis_Info($co_idx);
				if($data) {
					extract($data);
					if($co_satis) {
						$percent = 100 - ($before_satis / $co_satis * 100);
						$percent = round($percent,1)."%";
					}else{
						$co_satis = "-";
						$percent = "-";			
					}
				}			
				if($percent == "100%") $percent = "-";

				$comment_order='';
				$comment_order = $C_Course->Course_Comment_Order($co_idx);


				if($comment_order["cc_mem_level"] == 9){
					$comment ="<span class='success'>답변완료</span>";
				}else if($comment_order["cc_mem_level"] == 3){
					$comment ="<span class='wait'>답변대기</span>";
				}
				
				
		?>
			<li><a href="schedule.detail.php?co_idx=<?=$co_idx?>" class="panel">
				<dl>
					<dt class="no">No</dt><dd class="no" id="total_result"><?=$k--?></dd>
					<dt>강의일시</dt><dd class="date"><span class="nowrap"><?=$co_start_date?></span> <span class="nowrap"><?=$co_end_date?></span></dd>
					<dt>분야</dt><dd class="cate"><?=$co_field?></dd>
					<dt class="text-ir">제목</dt><dd class="subject">
						<span class="text"><?=$co_title?></span>
						<?=$new_icon?>
						<?=$comment?>
					</dd>
					<dt>향상도</dt><dd class="imp"><?=$percent?></span></dd>
					<dt>만족도</dt><dd class="sat"><?=$co_satis?></span></dd>
				</dl>
			</a></li>                                
				<?
				
			}
		 ?>
					
<script>
<? if(data_list_cnt >= $total_cnt) {?>
	$('.section.lectures-more.bxsdw').addClass('none');
<? }else{ ?>
	$('.section.lectures-more.bxsdw').removeClass('none');
<? } ?>

</script>                    