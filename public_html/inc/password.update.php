<?php 
include_once("../_init.php");
include_once "./head.php"; ?>
</head>
<body>
<?php include_once "./header.php"; ?>
	<div id="container" class="edu-schedule">
		<div id="top">
			<p class="title">비밀번호변경</p>
		</div>
		<div id="article">
			<div id="contact">
			<div class="contain bxsdw">
				<div class="body">
					<form  class="entry">
                    	<input type="hidden" id="code" value="<?=$_GET["key"]?>"
						<dl>
							<dt>비밀번호</dt>
							<dd><label class="i-label">
								<span class="i-placeholder">비밀번호를 입력해 주세요</span>
                                <input type="password" id="mem_pwd" class="i-text" name="mem_pwd">
							</label></dd>
							<dt>비밀번호 재입력</dt>
							<dd><label class="i-label">
								<span class="i-placeholder">비밀번호를 입력해 주세요</span>
								<input type="password" id="mb_rpwd" class="i-text" name="mb_rpwd">
							</label></dd>
						</dl>
						<button type="button" class="btn-submit" id="pw_submit">수정</button>
					</form>
				</div>
			</div>
		</div>
	</div>
<?php include_once "./footer.php"; ?>
</body>
</html> 
<script>
	$("#pw_submit").click(function(){
		var pwd = $("#mem_pwd").val();
		var rpwd = $("#mb_rpwd").val();
		var code = $("#code").val();
		console.log(pwd+"//"+rpwd);
		if(pwd==""||rpwd==""||pwd!=rpwd){
			alert("비밀번호 설정에 오류가 있습니다. 다시확인해주세요");
			return false;
		}
		$.ajax({
			type: "POST",
			url: "pw.update_proc.php",
			data: "pwd="+ pwd + "&code="+code,
			dataType: "text",
			success: function(msg) {
				if($.trim(msg) == "true") {
					alert("정상적으로 변경되었습니다.");
					location.href="/";
					return false;
				}else{
					alert('오류가 있습니다. 관리자에게 문의하세요');
					return;
				}				
			},
			error: function(xhr, status, error) { alert(error); }
		});
	});
</script>
	