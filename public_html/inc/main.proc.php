<?
	include_once($GP -> CLS."class.jhboard.php");
	$C_JHBoard = new JHBoard();
	
	//메인 최신글
	function Main_Notice($jb_code) {
		global $GP, $C_JHBoard, $C_Func;
		$limit_m = $jb_code == "10" ? "6" : ($jb_code == "20" ? "5" : ($jb_code == "30" ? "3" : ""));
		$args = array();
		$args['jb_code'] = $jb_code;
		$args['main_show2'] = "B";  //게시/비게시
		$args['order']		= "order by A.jb_order ASC, A.jb_depth DESC";		
		$args['limit']  = " limit 0, $limit_m";
		$rst = $C_JHBoard->Board_Main_Data($args);

		$str = "";
		for($i=0; $i<count($rst); $i++) {
			$jb_idx			= $rst[$i]['jb_idx'];
			$jb_file_name	= $rst[$i]['jb_file_name'];
			$jb_file_code	= $rst[$i]['jb_file_code'];
			$jb_code		= $rst[$i]['jb_code'];
			$jb_name 		= $rst[$i]['jb_name'];
			$jb_front_image = $rst[$i]['jb_front_image'];
			$jb_title 		= $C_Func->strcut_utf8($rst[$i]['jb_title'], 30, true, "...");
			$jb_reg_date 	= date("Y.m.d", strtotime($rst[$i]['jb_reg_date']));
			$jb_mb_id		= $C_Func->blindInfo($rst[$i]['jb_mb_id'],3);
			$jb_content		= $C_Func->dec_contents_edit($rst[$i]['jb_content']);
			$jb_content		= trim(strip_tags($jb_content));
			$jb_content 	= $C_Func->strcut_utf8($jb_content, 100, true, "...");	//제목 (길이, HTML TAG제한여부 처리)

			//타이틀이미지
			$new_image = " <img src=\"" . $GP -> IMG_PATH . "/skin/basic/image/ticon_new.gif\" border='0' align='middle'>";
			$new_icon = $C_Func->new_icon(1, $rst[$i]['jb_reg_date'], $new_image);

			$jb_title = $jb_title . $new_icon;
			
			$url = "?jb_code=".$jb_code."&jb_idx=".$jb_idx;
			$img_src = '';
			if($jb_front_image != '') {
				$code_file = $GP->UP_IMG_SMARTEDITOR_URL. "/jb_${jb_code}/${jb_front_image}";
				$img_src = "<img src='" . $code_file. "' alt='" . $jb_title_ori . "'  class='block' >";	
			}else{	
				if($jb_code == 30){
					$img_src = "<img src='/public/images/no_img2.jpg' alt='이미지 없음' class='block'>";
				}else{
					$img_src = "<img src='/public/images/no_img.jpg' alt='이미지 없음' class='block'>";
				}
			}
			
			switch($jb_code) {
				case "10" :
					$str .= '
							<li><a href="/notice.list.php?'.$url.'" class="panel bxsdw">
								<div class="row">
									<dl class="col">
										<dt class="subject">'.$jb_title.'</dt>
										<dd class="date">'.$jb_reg_date.'</dd>
									</dl>
								</div>
							</a></li>
							';
					break;
				case "20" :
					$str .= '<li><a href="/program.list.php?'.$url.'">'.$img_src.'</a></li>	
							';
					break;
				case "30" :
					$strClass = '';
					$i == 0 && ($strClass = ' class="first"');
					$i == count($rst) - 1 && ($strClass = ' class="last"');
					$str .= '<li'.$strClass.'><a href="/gallery.list.php?'.$url.'" class="panel">
								<div class="picture">'.$img_src.'</div>
								<dl class="info">
									<dt class="subject"><span>'.$jb_title.'</span></dt>
									<dd class="date">'.$jb_reg_date.'</dd>
								</dl>
							</a></li>	
							';
					break;
			}
		
		}
		return $str;
	}
	
	

?>