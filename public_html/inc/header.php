<!--[if lt IE 9]>
<div id="ie-version"><p>고객님께서는 현재 Explorer 구형버전으로 접속 중이십니다. 이 사이트는 Explorer 최신버전에 최적화 되어 있습니다. <a href="http://windows.microsoft.com/ko-kr/internet-explorer/download-ie" target="_blank">Explorer 업그레이드 하기</a></p> <button type="button" class="version-close">X</button></div>
<![endif]-->
<?
	if($_SESSION["suserid"]) {
		include_once($GP -> CLS."/class.course.php");
		include_once($GP -> CLS."/class.member.php");
		$C_Member 	= new Member;
		$C_Course 	= new Course;	
		$MData = $C_Member->Mem_Info_ID_Satis($_SESSION["suserid"]);	
		$Meber_name = $MData["mb_name"];
		$Meber_satis = round($MData["satis"],2);	
		$Meber_field = $MData["mb_field"];
		$Meber_edu = $MData["mb_edu"];	
		$Meber_course = $MData["mb_course"];
	}
?>

<div id="wrap">
	<div id="header">
		<div class="hgroup">
			<h1 class="logo"><a href="/">
				<i class="ip-icon-logo"></i>
				<span class="text-ir"><strong>K</strong>orea <strong>A</strong>ccreditation for <strong>C</strong>areer</span>
			</a></h1>
			<div id="nav">
				<h2 class="trigger"><a href="javascript:void(0);">
					<i class="icon-menu"></i>
					<span class="text-ir">menu</span>
				</a></h2>
				<div class="group">
					<div class="overlay"></div>
					<div class="panel">
						<ul class="list">
							<li class="dp1"><a href="/#/home"><span>HOME</span></a></li>
							<li class="dp1"><a href="/#/intro"><span>조합소개</span></a></li>
							<li class="dp1"><a href="/#/program"><span>교육프로그램</span></a></li>
							<li class="dp1"><a href="/#/photo"><span>활동사진</span></a></li>
							<li class="dp1"><a href="/#/notice"><span>공지사항</span></a></li>
							<li class="dp1"><a href="/#/contact"><span>Contact</span></a></li>
						</ul>
<!-- 로그인 완료-->
					<? if($C_Func->is_login() == "1") { ?>
						<a href="javascript:logout();" class="btn-sign-teacher session-true">
							<span>로그아웃</span>
							<i class="ip-icon-sign-arrow"></i>
						</a>
<!-- //로그인 완료 -->
<!-- 로그인 이전 --><? }else{ ?>
						<a href="javascript:void(0)" class="btn-sign-teacher">
							<span>강사로그인</span>
							<i class="ip-icon-sign-arrow"></i>
						</a>
<!-- //로그인 이전 --><? } ?>
						<div class="aside">
							<div class="overlay"></div>
							<div class="contain">
								<h2 class="trigger"><a href="javascript:void(0)">
									<i class="ip-icon-aside-arrow"></i>
									<span class="text-ir"></span>
								</a></h2>
<!-- 로그인 완료-->  	<? if($C_Func->is_login() == "1") { ?>
								<div class="panel session-true">
									<dl class="header">
										<dt class="teacher">
											<div class="picture"><img src="/public/images/empty-user.jpg" alt="" class="block" /></div>
											<span class="name"><?=$Meber_name?></span>
										</dt>
										<dd class="rate">
											<small>평점</small>
											<span class="score"><?=$Meber_satis?></span>
										</dd>
									</dl>
									<ul class="links">
										<li><a href="schedule.list.php"><span>강의일정</span></a></li>
										<li><a href="lesson-file.php"><span>강의교안/교재</span></a></li>
										<li><a href="stats.php"><span>강의 만족도 통계</span></a></li>
									</ul>
									<div class="footer">
										<i class="ip-icon-aside-logo"></i>
									</div>
									<a href="javascript:void(0)" class="btn-close">
										<i class="ip-icon-aside-close"></i>
										<span class="text-ir">닫기</span>
									</a>
								</div>
<!-- //로그인 완료-->	<? }else{ ?>
<!-- 로그인 이전 -->
								<div class="panel">
									<dl class="contact">
										<dt>조합가입문의</dt>
										<dd><a href="mailto:hellokac@gmail.com">hellokac@gmail.com</a></dd>
									</dl>
									<div class="footer">
										<i class="ip-icon-aside-logo"></i>
									</div>
								</div>
<!-- //로그인 이전 --><? } ?>
							</div>
						</div>
						<a href="javascript:void(0)" class="btn-close">
							<i class="ip-icon-nav-close"></i>
							<span class="text-ir">닫기</span>
						</a>
					</div>
				</div>
			</div>
		</div>
		<hr class="break-line" />
	</div>
<!-- 로그인 이전 -->
	<div id="signin">
		<div class="overlay"></div>
		<div class="contain">
			<div class="header">
				<h2 class="title">강사 로그인</h2>
				<p class="explain">본 메뉴는 강사회원에게만 제공됩니다.</p>
			</div>
			<form id="logInForm" class="panel">
				<label class="i-label">
					<span class="i-placeholder">Username</span>
					<input type="text" id="mb_id" class="i-text" name="mb_id">
				</label>
				<label class="i-label">
					<span class="i-placeholder">Password</span>
					<input type="password" id="mb_pwd" class="i-text" name="mb_pwd">
				</label>
				<button type="button" class="btn-login" id="btnLogin"><span>로그인</span></button>
			</form>
			<div class="footer">
				<h2 class="title">회원가입 및 ID/PW 문의</h2>
				<a href="#" onClick="javascript:go_param('contact');" class="btn-contact"><span>문의하기</span></a>
				<p>또는 조합(<a href="mailto:hellokac@gmail.com">hellokac@gmail.com</a>)으로</p>
			</div>
		</div>
	</div>
<!-- //로그인 이전에만 노출 -->
