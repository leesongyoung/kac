	<div id="footer">
		<div class="fgroup">
			<div class="contain">
				<i class="ip-icon-footer-logo"></i>
				<dl class="info">
					<dt class="address">주소</dt><dd class="address">부산 해운대구 센텀동로 45, 6층</dd>
					<dt class="email">Email</dt><dd class="email"><a href="mailto:hellokac@gmail.com">hellokac@gmail.com</a></dd>
				</dl>
			</div>
		</div>
		<p class="copy">&copy;2017 KAC All Right Reserved</p>
	</div>
</div>
<? if($C_Func->is_login() == "1") { ?>
<script>
	function logout(){
		$.ajax({
			type: "POST",
			url: "/inc/logout.proc.php",
			data: "mode=logout",
			dataType: "text",
			success: function(msg) {
				if($.trim(msg) == "true") {
					location.reload();
				}else{
					alert(msg);
					return;
				}				
			},
			error: function(xhr, status, error) { alert(error); }
		});		
	}
</script>
<? }else{ ?>
<script>
	$("#btnLogin").click(function(){
		console.log("click");
	var formData = $("#logInForm").serialize();
		$.ajax({
			type: "POST",
			url: "/inc/login.proc.php",
			data: formData,
			dataType: "text",
			success: function(msg) {
				if($.trim(msg) == "true") {
					location.reload();
				}else{
					alert(msg);
					return;
				}				
			},
			error: function(xhr, status, error) { alert(error); }
		});
	});
</script>
<? } ?>