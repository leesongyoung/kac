<?php
	include_once  '../_init.php';
	include_once $GP -> CLS . 'class.login.php';
    include_once $GP -> CLS . 'class.member.php';
	$C_Member = new Member();
	$C_Login = new Login();

	$args				 = array();      
	$args['mb_id']	   = $_POST["mb_id"];
	$args['mb_password'] = md5(trim($_POST["mb_pwd"]));
	$rst = $C_Login -> userLogin_ID($args);			
	//비밀번호가 같다면
	if($rst['mb_password'] ==  md5(trim($_POST["mb_pwd"]))) {
		$_SESSION['suserid'] 	= $rst['mb_id'];
		$_SESSION['susername'] 	= $rst['mb_name'];
		$_SESSION['suserphone'] = $rst['mb_mobile'];
		$_SESSION['suseremail'] = $rst['mb_email'];
		$_SESSION['suserlevel'] = $rst['mb_level'];
		$_SESSION['susercode'] 	= $rst['mb_code'];
		$_SESSION['suserunion'] 	= $rst['mb_union'];
		//마지막 로그인 날짜, 마지막 로그인 아이피, 로그인 누적횟수 수정
		$args = array();
		$args['mb_id'] = $_POST['mb_id'];
		$args['mb_lastlogin_date'] = date('Y-m-d H:i:s');
		$args['mb_lastlogin_ip']   = $_SERVER['REMOTE_ADDR'];
		$result = $C_Login -> Mem_Login_history_ID($args);
		echo "true";
		exit;
	} else {
		echo "아이디 혹은 패스워드가 틀립니다.";
		exit;
	}

?>