<?php
	include_once("../_init.php");
	include_once($GP -> CLS."/class.member.php");	
	include_once($GP -> CLS . "class.mail.php");
	$C_Member 	= new Member;	
	$C_SendMail = new SendMail();

	$imsipwd = $_POST["mb_id"].$C_Func->generateRandomString();
	$imsipwd = md5($imsipwd);
	
	$args = array();
	$args["mb_id"] = $_POST["mb_id"];
	$args["mb_imsi_code"] = $imsipwd;	
	$rst = $C_Member->Member_Imsi_Update($args);

	$email_subject 	= "KAC 비밀번호 안내입니다.";
	$sender_email 	= $GP -> Admin_Email;
	$sender_name 	= $GP -> Admin_Name;
	$receive_email 	= $_POST["email"];
	$receive_name 	= $_POST["name"];
	$email_content	= "KAC 비밀번호 설정입니다. 요청을 완료하려면 아래링크를 클릭하세요<br><br><a href='http://hellokac.me/inc/password.update.php?key=".$imsipwd."'>비밀번호 설정</a><br><br> 아래링크로 이동하셔서 비밀번호를 설정해주시기 바랍니다. 감사합니다.";
		
	$C_SendMail -> setUseSMTPServer(false);
	$C_SendMail -> setSMTPServer($GP -> SMTP_IP, $GP -> SMTP_PORT);
	$C_SendMail -> setSMTPUser($GP -> SMTP_USER);
	$C_SendMail -> setSMTPPasswd($GP -> SMTP_PASS);

	$C_SendMail -> setSubject($email_subject);
	$C_SendMail -> setMailBody($email_content, true);
	$C_SendMail -> setFrom($sender_email, $sender_name);
	$C_SendMail -> addTo($receive_email, $receive_name);

	$sendRst = $C_SendMail->send();
	//print_r($sendRst);
	if($sendRst) echo "true"; else "false";
	
?>
