<?php
	include_once("../../_init.php");
	
	include_once($GP->CLS."class.button.php");
	include_once($GP -> CLS."/class.field.php");
	$C_Field 	= new Field;
	$C_Button 		= new Button;
	ini_set("memory_limit" , -1);
	
	$cf_idx = "1";
	$rst = $C_Field->Field_List($cf_idx);
	include_once($GP -> INC_ADM_PATH."/head.php");
	$cf_name = explode(",",$rst["cf_name"]);
	$chk_field = $C_Func -> makeCheckbox_Value($cf_name, 'cf_name', '', '', '150');
?>
<body>
<div class="Wrap"><!--// 전체를 감싸는 Wrap -->
		<? include_once($GP -> INC_ADM_PATH."/header.php"); ?>
		<div class="boxContentBody">

			<ul>				
				<li>
					<strong class="tit">강의분야&nbsp;&nbsp;&nbsp;&nbsp;</strong>
					<span><input type="text" name="add_field" id="add_field" class="input_text" size="30" /></span>
					<span><button id="btn_add" class="btnSearch ">추가</button></span>
				</li>
			</ul>

		</div>	
		<div id="BoardHead" class="boxBoardHead">				
				<div class="boxMemberBoard">
					<table>
						<colgroup>
							<col />
							<col />
							<col />
							<col />
							<col />
							<col />
							<col />
							<col />
							<col style="width:101px;" />
						</colgroup>
						<tbody>
                        	<tr>
                            	<td>
                            	<div id="field_result">
                                	<?=$chk_field?>
                                </div>
                            	</td>
                            </tr>
						</tbody>
					</table>
				</div>			
			</div>
            <div class="btnWrap">
            	<button id="del_submit" class="btnSearch ">삭제</button>
                <button id="img_submit" class="btnSearch ">저장</button>
            </div>            
		</div>
		<? include_once($GP -> INC_ADM_PATH."/footer.php"); ?>
	</div>
</div><!-- 전체를 감싸는 Wrap //-->
</body>
</html>
<script type="text/javascript">
	$(document).ready(function(){	
		$("#btn_add").click(function(){
			var chv = $("#add_field").val();
			if(!chv) {alert("강의분야를 입력하세요");$("#add_field").focus();return false;}
			var html = "<div style='display:inline-block; line-height:20px; width:150px;'><input type='checkbox' name='cf_name' value='"+chv+"'> "+chv+"</div>"; 
			$("#field_result").append(html);
			$("#btn_add").val('');			
		});
		$("#del_submit").click(function(){
			if(!confirm("삭제하시겠습니까?")) return;
			$(":checkbox[name='cf_name']:checked").each(function() {
				$(this).parent("div").remove().empty();
			});
			
		});
		$("#img_submit").click(function(){
			if(!confirm("저장하시겠습니까?")) return;
			var cf_name = "";
			$(":checkbox[name='cf_name']").each(function() {
				cf_name += ","+$(this).val();
			});
		    if(cf_name!="") {
				cf_name = cf_name.substring(1);
					$.ajax({
						type: "POST",
						url: "field_reg.php",
						data:{
							"cf_name" : cf_name
						},
						dataType: "text",
						success: function(data) {
							console.log(data);
						},
						error: function(xhr, status, error) { alert(error); }
					});				
			}
			//console.log(data);
		});		
	});
</script>