<?php
// Dedc : admin 메뉴 Array
// Writer :
$GP -> MENU_ADMIN = array(
		array("tab"=>"1",			"folder"=>"main", 			"name"=>"관리자정보",			"link"=> "/admin/main/adm_info.php?m_tab=1"),
		array("tab"=>"2",			"folder"=>"bbs", 			"name"=>"게시판관리",			"link"=> "/admin/bbs/bbs_list.php?m_tab=2"),
		array("tab"=>"3",			"folder"=>"member", 		"name"=>"강사등록관리",				"link"=> "/admin/member/mem_list.php?m_tab=3"),

		array("tab"=>"8",			"folder"=>"member_satis", 		"name"=>"강사만족도관리",				"link"=> "/admin/member_satis/satis_list.php?m_tab=8"),

		array("tab"=>"4",			"folder"=>"field", 			"name"=>"강의분야관리",			"link"=> "/admin/field/field_list.php?m_tab=4"),
		array("tab"=>"5",			"folder"=>"course", 		"name"=>"강의일정관리",		"link"=> "/admin/course/course_list.php?m_tab=5"),
		array("tab"=>"7",			"folder"=>"stats", 			"name"=>"강의만족도통계",				"link"=> "/admin/stats/stats_list.php?m_tab=7"),
		array("tab"=>"6",			"folder"=>"popup", 			"name"=>"팝업관리",				"link"=> "/admin/popup/popup_list.php?m_tab=6"),
);


$GP -> MENU_SUB_ADMIN = array();

$GP -> MENU_SUB_ADMIN['main'] = array(
	"시스템관리"   => array(
		array("tab"=>"1",		"title"=>"main1",		"name"=>"관리자 정보",		"link"=>  "/admin/main/adm_info.php"),
		array("tab"=>"2",		"title"=>"main2",		"name"=>"권한 정보",			"link"=>  "/admin/main/adm_auth.php"),
	)
);

$GP -> MENU_SUB_ADMIN['bbs'] = array(
	"게시판관리"   => array(
		array("tab"=>"1",		"title"=>"bbs1",		"name"=>"게시판 리스트",				"link"=>  "/admin/bbs/bbs_list.php"),
	)
);
$GP -> MENU_SUB_ADMIN['member'] = array(
	"회원관리"   => array(
		array("tab"=>"1",	"title"=>"member1",		"name"=>"강사 리스트",							"link"=>  "/admin/member/mem_list.php")
	)
);

$GP -> MENU_SUB_ADMIN['member_satis'] = array(
	"회원관리"   => array(
		array("tab"=>"1",	"title"=>"member_satis",		"name"=>"강사 만족도 리스트",							"link"=>  "/admin/member_satis/satis_list.php")
	)
);

$GP -> MENU_SUB_ADMIN['field'] = array(
	"강의분야관리"   => array(
		array("tab"=>"1",	"title"=>"field1", "name"=>"강의분야관리리스트",			"link"=>  "/admin/field/field_list.php"),
	)
);
$GP -> MENU_SUB_ADMIN['course'] = array(
	"강의일정관리"   => array(
		array("tab"=>"1",	"title"=>"course1", "name"=>"강의일정",			"link"=>  "/admin/course/course_list.php"),
		array("tab"=>"2",	"title"=>"course2", "name"=>"Active",			"link"=>  "/admin/course/course_new_list.php"),
	)
);

$GP -> MENU_SUB_ADMIN['popup'] = array(
	"팝업관리"   => array(
		array("tab"=>"1",	"title"=>"popup1",		"name"=>"팝업 리스트",				"link"=>  "/admin/popup/popup_list.php"),
	)
);

$GP -> MENU_SUB_ADMIN['stats'] = array(
	"강의만족도통계"   => array(
		array("tab"=>"1",	"title"=>"stats1",		"name"=>"강의만족도통계",				"link"=>  "/admin/popup/stats_list.php"),
	)
);

?>