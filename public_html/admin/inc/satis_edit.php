<?php
	include_once("../../_init.php");
	include_once($GP -> INC_ADM_PATH."/head.php");	
	include_once($GP -> CLS."/class.course.php");	
	$C_Course 	= new Course;

	$co_idx = $_GET['co_idx'];
	$data = $C_Course->Course_Satis_Info($co_idx);	
	
	if($data) {
		extract($data);
	}
?>
<body>
<div class="Wrap_layer"><!--// 전체를 감싸는 Wrap -->
	<div class="boxContent_layer">
		<div class="boxContentHeader">
			<span class="boxTopNav"><strong>강의 등록</strong></span>
		</div>
		<form name="base_form" id="base_form">
		<input type="hidden" id="old_satis" value="<?=$before_satis?>" /> 
		<input type="hidden" id="co_idx" value="<?=$co_idx?>" />                        
		<div class="boxContentBody">			
			<div class="boxMemberInfoTable_layer">				
				<div class="layerTable">
				<table class="table table-bordered">
					<tbody>
	                    <tr>
							<th width="30%">강사명</th>
							<td width="70%">
								<?=$co_mem_name?>
							</td>
						</tr>
						<tr>
							<th>종전 만족도</th>
							<td>
							  <font style="color:#09F; font-weight:bold;"><? if($before_satis) echo $before_satis; else echo "없음";?></font> / 5. 0
							</td>
						</tr>
						<tr>
							<th><span>*</span>강의 만족도</th>
							<td>
                                <input type="text" class="input_text" size="25" name="co_satis" id="co_satis" value="<?=$co_satis?>"/> / 5.0           
							</td>
						</tr>
						<tr>
							<th><span>*</span>강의 향상도</th>
							<td>
                                <input type="text" class="input_text" size="25" name="co_improve" id="co_improve" value="<?=$co_improve?>"/> / 5.0           
							</td>
						</tr>
						<!--tr>
							<th>향상도</th>
							<td id="persent">
								
							</td>
						</tr-->
					</tbody>
				</table>
				</div>
				<div class="btnWrap">
				<button id="img_submit" class="btnSearch ">등록</button>
				<button id="img_cancel" class="btnSearch " onClick="javascript:parent.modalclose();">취소</button>
				</div>
			</div>
		</div>
		</form>
	</div>
</div>
</body>
</html>
<script type="text/javascript">
	$(document).ready(function(){	
		$('#img_submit').click(function(){
			var satis = $("#co_satis").val();
			var improve = $("#co_improve").val();
			var co_idx = $("#co_idx").val();
			if(satis == "") {
				alert("평가를 입력해주세요");
				return false;
			}
			console.log(satis);
			if(satis > 5) {
				alert("평가는 5.0 이상이 될 수 없습니다.");
				return false;
			}
			$.ajax({
				type: "POST",
				url: "/admin/member/satis_update.php",
				data: "satis=" + satis + "&co_idx=" + co_idx+ "&improve=" + improve,
				dataType: "text",
				success: function(data) {
					console.log(data);
					if(data) {
						alert("수정되었습니다.");	
					}else{
						alert("오류가 발생했습니다. 관리자에게 문의하세요");						
					}
					//parent.modalclose();
					location.reload();
				},
				error: function(xhr, status, error) { 
					//alert(error); 
				}
			});
			parent.modalclose();
		});
		$("#co_satis").keyup(function(){ 
			var satis = $(this).val();
			var mb_satis = $("#old_satis").val();
			satis    	 = Number(satis);
			mb_satis     = Number(mb_satis);
			if(satis == "" || mb_satis == 0) {
				$("#persent").empty();
			}
			if(satis > 5.0) {
				console.log(satis);
				alert("최대점수는 5.0입니다."); $("#persent").empty(); $("#co_satis").val(""); return;
			}else{
				if(mb_satis != "") {
					rst = 100 - ( mb_satis / satis * 100);
					$("#persent").empty().append(Math.ceil(rst)+" %");
				}
			}
			//console.log(rst);
		});
	});
</script>