<?php
	include_once("../../_init.php");
	include_once($GP -> INC_ADM_PATH."/head.php");	
	include_once($GP -> INC_ADM_PATH."inc.adm_auth.php");
	include_once($GP -> CLS."/class.member.php");
	include_once($GP -> CLS."/class.field.php");	
	$C_Member 	= new Member;
	$C_Field 	= new Field;
	
	$args = array();
	$args['mb_code'] = $_GET['mb_code'];
	$data = $C_Member->Mem_Info($args);
	
	if($data) {
		extract($data);
		$cf_idx = "1";
		$field_list = $C_Field->Field_List($cf_idx);	
		$cf_name = explode(",",$field_list["cf_name"]);	
		
		$arr_mobile = explode("-", $mb_mobile);
		$mb_mobile1 = $arr_mobile[0];
		$mb_mobile2 = $arr_mobile[1];
		$mb_mobile3 = $arr_mobile[2];
		
		$arr_eamil = explode("@", $mb_email);
		$mb_email1 = $arr_eamil[0];
		$mb_email2 = $arr_eamil[1];
		$mb_field = explode(",",$mb_field);
		$chk_field = $C_Func -> makeCheckbox_Value($cf_name, 'mb_field[]', $mb_field, '', '150');
		$sel_mobile = $C_Func -> makeSelect('mb_mobile1', $GP -> MOBILE, $mb_mobile1, '', '::선택::');		
	}
?>

<body>
<div class="Wrap_layer"><!--// 전체를 감싸는 Wrap -->
	<div class="boxContent_layer">
		<div class="boxContentHeader">
			<span class="boxTopNav"><strong>회원정보 수정</strong></span>
		</div>
		<form name="base_form" id="base_form" method="POST" action="?" enctype="multipart/form-data">
		<input type="hidden" name="mode" id="mode" value="MEM_MODI" />
		<input type="hidden" name="mb_code" id="mb_code" value="<?=$_GET['mb_code']?>" />
		<input type="hidden" name="mb_satis" id="mb_satis" value="<?=$mb_satis?>" />
		<div class="boxContentBody">
			<div class="boxMemberInfoTable_layer">
				<div class="layerTable">
				<table class="table table-bordered">
					<tbody>
						<tr>
							<th width="15%"><span>*</span>회원아이디</th>
							<td width="85%">
								<input type="text" class="input_text" size="25" name="mb_id" id="mb_id" value="<?=$mb_id?>" readonly/>
                                <?=$union_select?>
							</td>
						</tr>
						<tr>
							<th width="15%"><span>*</span>회원명</th>
							<td width="85%">
								<input type="text" class="input_text" size="25" name="mb_name" id="mb_name" value="<?=$mb_name?>" />
							</td>
						</tr>
						<!--tr>
							<th width="15%"><span>*</span>만족도</th>
							<td width="85%">
								<input type="text" class="input_text" size="25" name="mb_satis" id="mb_satis" value="<?=$mb_satis?>" /> / 5.0
							</td>
						</tr-->   
						<tr>
							<th width="15%"><span>*</span>강의분야</th>
							<td width="85%">
                                <?=$chk_field?>
							</td>
						</tr> 
						<tr>
							<th><span>*</span>대표강의</th>
							<td>
								<textarea name="mb_course" id="mb_course" style="width:98%; height:100px;  overflow:auto;" ><?=$mb_course?></textarea>
							</td>
						</tr>	
						<tr>
							<th width="15%"><span>*</span>학력</th>
							<td width="85%">
								<input type="text" class="input_text" size="25" name="mb_edu" id="mb_edu" value="<?=$mb_edu?>" />
							</td>
						</tr>    
						<tr>
							<th width="15%"><span>*</span>생년월일</th>
							<td width="85%">
								<input type="text" class="input_text" size="25" name="mb_birthday" id="mb_birthday" value="<?=$mb_birthday?>" />
							</td>
						</tr                                                                                   
                        
						><tr>
							<th><span>*</span>이메일</th>
							<td>
								<input type="text" class="input_text" size="25" name="mb_email1" id="mb_email1" value="<?=$mb_email1?>" /> @
								<input type="text" class="input_text" size="25" name="mb_email2" id="mb_email2" value="<?=$mb_email2?>" /> 
                                <?=$sel_email?>                             
                                <a class="btnSearch" id="pw_link" style="cursor:pointer;">PW 링크발송</a>
							</td>
						</tr>
						<tr>
							<th><span>*</span>연락처</th>
							<td>
									<?=$sel_mobile?>-
									<input type="text" id="mb_mobile2" name="mb_mobile2" size="4" value="<?=$mb_mobile2?>" maxlength="4" class="input_text" style="width:70px;"/>-
									<input type="text" id="mb_mobile3" name="mb_mobile3" size="4" value="<?=$mb_mobile3?>" maxlength="4" class="input_text" style="width:70px;"/>
									<span class="my_error_display"></span>
							</td>
						</tr>                        
						<tr>
							<th><span>*</span>주소</th>
							<td>	
								<div style="margin-top:3px;"><input type="text" name="mb_zip_code" id="mb_zip_code" value="<?=$mb_zip_code?>" size="10" class="input_text" />
              					<button class="btnSearch" id="search_btn">우편번호</button>
								</div>
								<div style="margin-top:3px;"><input type="text" name="mb_address1" id="mb_address1" value="<?=$mb_address1?>" placeholder=" * 주소가 자동입력됩니다." size="100" class="input_text" /></div>
								<div style="margin-top:3px;"><input type="text" name="mb_address2" id="mb_address2" value="<?=$mb_address2?>"  placeholder=" * 상세주소를 입력하십시오." size="100" class="input_text" /></div>
								<span class="my_error_display"></span>
							</td>
						</tr>

						<tr>
							<th><span>*</span>계좌번호</th>
							<td>
								<input type="text" class="input_text" size="25" name="mb_bank_no" id="mb_bank_no" value="<?=$mb_bank_no?>" />
                                <input type="text" class="input_text" size="10" name="mb_bank" id="mb_bank" value="<?=$mb_bank?>" /> 은행
							</td>
						</tr>
					</tbody>
				</table>
				</div>
				<div class="btnWrap">
				<button id="img_submit" class="btnSearch ">수정</button>
				<button id="img_cancel" class="btnSearch ">취소</button>
				</div>
			</div>
		</div>
		</form>
	</div>
</div>
</body>
</html>
<script src="/admin/js/jquery.alphanumeric.js" type="text/javascript"></script>
<script type="text/javascript" src="/admin/js/jquery.validate.js"></script>
<script type="text/javascript">
	$(document).ready(function(){	
		$('#search_btn').click(function(){
			window.open('/inc/address_pop.html?obj=mb_zip_code&obj2=mb_address1&obj3=mb_address2', 'ifm_addr', 'width=500,height=600,resizable=yes,scrollbars=no,status=no,toolbar=no' );
			return false;
		});															 
		$('#m_mobile2').numeric();
		$('#m_mobile3').numeric();							   
		$('#img_submit').click(function(){					
			$('#base_form').submit();
			return false;
		});
		
		$('#img_cancel').click(function(){
				parent.modalclose();				
		});

		$('#pw_link').click(function(){
			var email1 = $("#mb_email1").val();
			var email2 = $("#mb_email2").val();
			var mb_id = $("#mb_id").val();		
			if(!mb_id) {
				alert("아이디를 입력해주세요");	return false;	
			}
			if(email1 == "" || email2 == "") {
				alert("이메일을 입력해주세요"); return false;	
			}
			if (!confirm(mb_id+" 아이디로 보내시겠습니까?")) return false;        
			$.ajax({
				type: "POST",
				url: "/inc/pw.send.php",
				data: "mb_id="+mb_id+"&email=" + email1 + "@" + email2,
				dataType: "text",
				success: function(msg) {
					console.log(msg);
					if($.trim(msg) == "true") {
						alert("정상적으로 발송 했습니다.");
						return false;
					}else{
						alert('발송에 실패하였습니다.');
						return;
					}				
				},
				error: function(xhr, status, error) { alert(error); }
			});
		});
		
		$.validator.addMethod("emailcheck", function(value, element) {
			var val = value;
			return this.optional(element) || /^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i.test(val);
		
		}, jQuery.validator.messages.emailcheck);
					
			$('#base_form').validate({
			rules: {	
				mb_id: { required: true},
				mb_name: { required: true},			
				mb_satis: { required: true},			
				mb_email: { required: true, emailcheck:true},
				mb_birthday : { required:true },
				mb_address1 : {required :true },
				mb_address2 : {required :true },
				m_mobile2 : {required :true },
				m_mobile3 : {required :true }				
			},
			messages: {	
				mb_id: { required: "조합원 여부를 선택하세요" },			
				mb_name: { required: "회원명을 입력하세요" },
				mb_satis: { required: "만족도를 입력하세요" },				
				mb_email: { required: "이메일을 입력하세요", emailcheck:"올바른 이메일을 입력하세요" },			
				mb_birthday: { required: "생년월일을 입력하세요" },
				mb_address1: { required: "주소를 입력하세요" },
				mb_address2: { required: "상세주소를 입력하세요" },
				m_mobile2: { required: "전호번호를 입력하세요" },
				m_mobile3: { required: "전호번호를 입력하세요" }
			},
			onkeyup:false,
			onclick:false,
			onfocusout:false,			
			showErrors: function(errorMap, errorList) {
				if(!$.isEmptyObject(errorList)){
	     		//      var caption = $(errorList[0].element).attr('caption') || $(errorList[0].element).attr('name');
				//		alert(errorList[0].message);
				}
			},
			submitHandler: function(frm) {
			if (!confirm("수정하시겠습니까?")) return false;                
				frm.action = './proc/mem_proc.php';
				frm.submit(); //통과시 전송
				return true;
			},

			success: function(element) {
			//
			}
		
		});
		
	});
</script>
