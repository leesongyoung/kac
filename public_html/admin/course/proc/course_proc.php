<?php
include_once("../../../_init.php");
include_once($GP -> CLS."/class.course.php");
include_once($GP -> CLS."/class.member.php");
$C_Member 	= new Member;
$C_Course 	= new Course;

switch($_POST['mode']){
	case 'COURSE_MODI':
		if (is_array($_POST)) foreach ($_POST as $k => $v) ${$k} = $v;		
		$file_save_path = $GP -> UP_COURSE;
		//에디터
		if($img_full_name != "") {
			$Arr_img = explode(',', $img_full_name);	
			$img_name = "";
			for	($i=0; $i<count($Arr_img); $i++) {		
				if(ereg($C_Func->escape_ereg($Arr_img[$i]), $C_Func->escape_ereg($co_contents))) {		
					$img_name .= trim($Arr_img[$i]) . ",";		
				}else{
					@unlink($file_save_path . $Arr_img[$i]);
				}
			}
			$img_name = rtrim($img_name , ",");
			
			$args['editor_img_code'] = $img_name;
		}
		
		$co_mem_name = $C_Member -> Mem_Info_ID($co_mem_id);
	
		$args = array();
		$args['co_idx'] 			= $co_idx;		
		$args['co_field'] 			= $co_field;
		$args['co_title'] 			= $co_title;
		$args['co_mem_id'] 			= $co_mem_id;		
		$args['co_mem_name'] 		= $co_mem_name["mb_name"];				
		$args['co_agency'] 			= $co_agency;		
		$args['co_area'] 			= $co_area;
		$args['co_target'] 			= $co_target;
		$args['co_place'] 			= $co_place;
		$args['co_start_date']		= $co_start_date." ".$co_start_time;
		$args['co_end_date']		= $co_end_date." ".$co_end_time;		
		$args['co_contents'] 		= $C_Func->enc_contents($co_contents);;		
		$rst = $C_Course -> Course_Info_Modify($args);
		$C_Func->put_msg_and_modalclose("수정 되었습니다");		
		exit();
	break;
	
	case 'COURSE_REG':
		if (is_array($_POST)) foreach ($_POST as $k => $v) ${$k} = $v;		
		
		$file_save_path = $GP -> UP_COURSE;
		//에디터
		if($img_full_name != "") {
			$Arr_img = explode(',', $img_full_name);	
			$img_name = "";
			for	($i=0; $i<count($Arr_img); $i++) {		
				if(ereg($C_Func->escape_ereg($Arr_img[$i]), $C_Func->escape_ereg($co_contents))) {		
					$img_name .= trim($Arr_img[$i]) . ",";		
				}else{
					@unlink($file_save_path . $Arr_img[$i]);
				}
			}
			$img_name = rtrim($img_name , ",");
			
			$args['editor_img_code'] = $img_name;
		}
		
		$co_mem_name = $C_Member -> Mem_Info_ID($co_mem_id);
	
		$args = array();
		$args['co_field'] 			= $co_field;
		$args['co_title'] 			= $co_title;
		$args['co_mem_id'] 			= $co_mem_id;		
		$args['co_mem_name'] 		= $co_mem_name["mb_name"];				
		$args['co_agency'] 			= $co_agency;		
		$args['co_area'] 			= $co_area;
		$args['co_target'] 			= $co_target;
		$args['co_place'] 			= $co_place;
		$args['co_start_date']		= $co_start_date." ".$co_start_time;
		$args['co_end_date']		= $co_end_date." ".$co_end_time;		
		$args['co_contents'] 		= $C_Func->enc_contents($co_contents);;		

		$rst = $C_Course -> Course_Reg($args);
		$C_Func->put_msg_and_modalclose("등록 되었습니다");		
		exit();
	break;	


	case 'COURSE_DEL' :
		if (is_array($_POST)) foreach ($_POST as $k => $v) ${$k} = $v;
		
		$args = array();
		$args['co_idx'] 	= $co_idx;
		
			$rst = $C_Course -> Course_Info_Del($args);

		echo "true";
		exit();
	break;
	
}
?>
