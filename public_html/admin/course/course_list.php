<?php
	include_once("../../_init.php");
	include_once($GP -> INC_ADM_PATH."/head.php");
	
	include_once($GP->CLS."class.list.php");
	include_once($GP -> CLS."/class.course.php");	
	include_once($GP->CLS."class.button.php");
	$C_ListClass 	= new ListClass;
	$C_Course 	= new Course;
	$C_Button 		= new Button;
	
	$args = array();
	
	if (!$_GET['s_date']) {
		$s_date = date("Y-m-d");		
	}else{
		$s_date = $_GET['s_date'];		
	}
	if (!$_GET['e_date']) {
		$e_date = date("Y-m-d");		
	}else{
		$e_date = $_GET['e_date'];		
	}



	$args['show_row'] = 15;
	$args['pagetype'] = "admin";
	$args['order'] = "co_start_date desc";
	$data = "";
	$data = $C_Course->Course_List(array_merge($_GET,$_POST,$args));
	
	$rst = $C_Course->Course_New_Reply_Cnt();	
	$ncnt = $rst["cnt"];
	$data_list 		= $data['data'];
	$page_link 		= $data['page_info']['link'];
	$page_search 	= $data['page_info']['search'];
	$totalcount 	= $data['page_info']['total'];
	
	$totalpages 	= $data['page_info']['totalpages'];
	$nowPage 		= $data['page_info']['page'];
	$totalcount_l 	= number_format($totalcount,0);
	
	$data_list_cnt 	= count($data_list);
?>
<body>
<div class="Wrap"><!--// 전체를 감싸는 Wrap -->
		<? include_once($GP -> INC_ADM_PATH."/header.php"); ?>
		<div class="boxContentBody">
			<div class="boxSearch">
			<!--? include_once($GP -> INC_ADM_PATH."/inc.mem_search.php"); ?-->										
			<form name="base_form" id="base_form" method="GET">
			<ul>				
				
				<strong class="tit">등록일</strong>
				<span><input type="text" name="s_date" id="s_date" value="<?=$_GET['s_date']?>" class="input_text" size="13"></span>
				<span>~</span>
				<span><input type="text" name="e_date" id="e_date" value="<?=$_GET['e_date']?>" class="input_text" size="13" /></span>
				<li>
				<strong class="tit">검색조건</strong>
				<span>
				
					<select name="search_key" id="search_key">
						<option value="">:: 선택 ::</option>
						<option value="co_field" <? if($_GET['search_key'] == "co_field"){ echo "selected";}?> >강의분야</option>
						<option value="co_title" <? if($_GET['search_key'] == "co_title"){ echo "selected";}?> >강의명</option>
						<option value="co_contents" <? if($_GET['search_key'] == "co_contents"){ echo "selected";}?> >내용</option>
						<option value="all" <? if($_GET['search_key'] == "all"){ echo "selected";}?> >강의명/내용</option>
						<option value="co_mem_name" <? if($_GET['search_key'] == "co_mem_name"){ echo "selected";}?> >강사명</option>
                                                                                                                        
					</select>
					</span>
					<span><input type="text" name="search_content" id="search_content" value="<?=$_GET['search_content']?>" class="input_text" size="16" /></span>
					<span><button id="search_submit" class="btnSearch ">검색</button></span>
				</li>
			</ul>
			</form>
			</div>
		</div>	

		<div style="margin-top:5px; text-align:right;">
		<button onClick="layerPop('ifm_reg','./course_reg.php', '100%', 700)"; class="btnSearch ">강의 등록</button>
		</div>

		<div id="BoardHead" class="boxBoardHead">				
				<div class="boxMemberBoard">
					<table>
							<col />
							<col />
							<col />
							<col />
							<col />
							<col style="width:151px;" />
						<thead>
							<tr>
								<th>No</th>								
								<th>일정</th>								
								<th>강의분야</th>								
								<th>강의명</th>								
								<th>강사명</th>								
								<th>수정/삭제</th>
							</tr>
						</thead>
						<tbody>
							<?
								$dummy = 1;
								for ($i = 0 ; $i < $data_list_cnt ; $i++) {
									$co_idx 		= $data_list[$i]['co_idx'];
									$co_title		= $data_list[$i]['co_title'];
									$co_field		= $data_list[$i]['co_field'];
									$co_mem_id		= $data_list[$i]['co_mem_id'];
									$co_mem_name	= $data_list[$i]['co_mem_name'];
									$co_start_date	= date("Y.m.d H:i:s", strtotime($data_list[$i]['co_start_date']));
									$co_end_date	= date("Y.m.d H:i:s", strtotime($data_list[$i]['co_end_date']));
									
									$edit_btn = $C_Button -> getButtonDesign('type2','수정',0,"layerPop('ifm_reg','./course_edit.php?co_idx=" . $co_idx. "', '100%', 700)", 50,'');	
									//$edit_btn .= $C_Button -> getButtonDesign('type2','설정',0,"layerPop('ifm_reg','/admin/inc/satis_edit.php?mb_id=" . $co_mem_id. "', '100%', 350)", 50,'');	
									$edit_btn .= $C_Button -> getButtonDesign('type2','설정',0,"layerPop('ifm_reg','/admin/inc/satis_edit.php?co_idx=" . $co_idx. "', '100%', 350)", 50,'');										
									$edit_btn .= $C_Button -> getButtonDesign('type2','삭제',0,"course_delete('" . $co_idx. "')", 50,'');							
								?>
										<tr>
											<td><?=$data['page_info']['start_num']--?></td>											
											<td><?=$co_start_date?> ~ <?=$co_end_date?></td>
											<td><?=$co_field?></td>											
											<td><a href="course_view.php?co_idx=<?=$co_idx?>"><?=$co_title?></a></td>
											<td><?=$co_mem_name?></td>											
											<td><?=$edit_btn?></td>
										</tr>
										<?
										$dummy++;
									}
							?>						
						</tbody>
					</table>
				</div>			
			</div>
			<ul class="boxBoardPaging">
				<?=$page_link?>
			</ul>
		</div>
		<? include_once($GP -> INC_ADM_PATH."/footer.php"); ?>
	</div>
</div><!-- 전체를 감싸는 Wrap //-->
</body>
</html>
<script type="text/javascript">

	$(document).ready(function(){														 
		$("#ncnt > a").append(" (<?=$ncnt?>) ");
	
	
		callDatePick('s_date');
		callDatePick('e_date');

		$('#search_submit').click(function(){																			 

			if($.trim($('#search_content').val()) != '')
			{
				if($('#search_key option:selected').val() == '')
				{
					alert('검색조건을 선택하세요');
					return false;
				}
			}

			if($('#search_key option:selected').val() != '')
			{
				if($.trim($('#search_content').val()) == '')
				{
					alert('검색내용을 입력하세요');
					$('#search_content').focus();
					return false;
				}
			}


			$('#base_form').submit();
			return false;
		});

	});

	function course_delete(co_idx)
	{
		if(!confirm("삭제하시겠습니까?")) return;

		$.ajax({
			type: "POST",
			url: "./proc/course_proc.php",
			data: "mode=COURSE_DEL&co_idx=" + co_idx,
			dataType: "text",
			success: function(msg) {
				
				if($.trim(msg) == "true") {
					alert("삭제되었습니다");
					window.location.reload();
					return false;
				}else{
					alert('삭제에 실패하였습니다.');
					return;
				}				
			},
			error: function(xhr, status, error) { alert(error); }
		});

	}
</script>

<style>
#simplemodal-container {width:554px}
</style>