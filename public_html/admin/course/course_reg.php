<?php
	include_once("../../_init.php");
	include_once($GP -> INC_ADM_PATH."/head.php");	
	include_once($GP -> CLS."/class.course.php");	
	include_once($GP -> CLS."/class.field.php");		
	$C_Course 	= new Course;
	$C_Field 	= new Field;
	$cf_idx = "1";
	$field_list = $C_Field->Field_List($cf_idx);	
	$cf_name = explode(",",$field_list["cf_name"]);	
	
	$co_field = $C_Func -> makeSelect_Normal_Value('co_field', $cf_name, '', '', '::선택::');		
?>
<body>
<div class="Wrap_layer"><!--// 전체를 감싸는 Wrap -->
	<div class="boxContent_layer">
		<div class="boxContentHeader">
			<span class="boxTopNav"><strong>강의 등록</strong></span>
		</div>
		<form name="base_form" id="base_form" method="POST" action="?" enctype="multipart/form-data">
		<input type="hidden" name="mode" id="mode" value="COURSE_REG" />
		<div class="boxContentBody">			
			<div class="boxMemberInfoTable_layer">				
				<div class="layerTable">
				<table class="table table-bordered">
					<tbody>
	                    <tr>
							<th width="15%"><span>*</span>강의분야</th>
							<td width="85%">
								<?=$co_field?>
							</td>
						</tr>
						<tr>
							<th width="15%"><span>*</span>프로그램명</th>
							<td width="85%">
								<input type="text" class="input_text" size="25" name="co_title" id="co_title"/>
							</td>
						</tr>
						<tr>
							<th><span>*</span>강사</th>
							<td>
                                <select name="co_mem_id" id="co_mem_id">
                                    <option value="">강사 선택</option>
                                </select>                            
							</td>
						</tr>
						<tr>
							<th><span>*</span>대상기관</th>
							<td>
								<input type="text" class="input_text" size="25" name="co_agency" id="co_agency"/>
							</td>
						</tr>
						<tr>
							<th><span>*</span>지역</th>
							<td>
								<input type="text" class="input_text" size="25" name="co_area" id="co_area"/>
							</td>
						</tr>
						<tr>
							<th><span>*</span>대상</th>
							<td>
								<input type="text" class="input_text" size="25" name="co_target" id="co_target"/>
							</td>
						</tr>
						<tr>
							<th><span>*</span>강의장소</th>
							<td>
								<input type="text" class="input_text" size="25" name="co_place" id="co_place"/>
							</td>
						</tr>                                                                        
						<tr>
							<th><span>*</span>강의일정</th>
							<td>
								<input type="text" class="input_text" size="15" name="co_start_date" id="co_start_date" />
                                시간 <input type="text" class="input_text" size="10" name="co_start_time" id="co_start_time" />
								 ~ 
								<input type="text" class="input_text" size="15" name="co_end_date" id="co_end_date" />
                                시간 <input type="text" class="input_text" size="10" name="co_end_time" id="co_end_time" />                                
							</td>
						</tr>
						<tr id="TR1">
								<th><span>*</span>참고사항</th>
								<td>                          
									<textarea name="co_contents" id="co_contents" style="display:none"></textarea>
									<textarea name="ir1" id="ir1" style="width:100%; height:300px; min-width:280px; display:none;"></textarea>      
								</td>
						</tr>
					</tbody>
				</table>
				</div>
				<div class="btnWrap">
				<button id="img_submit" class="btnSearch ">등록</button>
				<button id="img_cancel" class="btnSearch ">취소</button>
				</div>
			</div>
		</div>
		</form>
	</div>
</div>
</body>
</html>
<script type="text/javascript" src="<?=$GP -> JS_PATH?>/jquery.alphanumeric.js"></script>
<script type="text/javascript" src="<?=$GP -> JS_PATH?>/jquery.validate.js"></script>
<script type="text/javascript" src="<?=$GP -> JS_SMART_PATH?>/HuskyEZCreator.js" charset="utf-8"></script>
<script type="text/javascript" src="<?=$GP -> JS_PATH?>/jquery.base64.js"></script>
<script type="text/javascript">

	var oEditors = [];
	nhn.husky.EZCreator.createInIFrame({
		oAppRef: oEditors,
		elPlaceHolder: "ir1",
		sSkinURI: "/bbs/smarteditor/SmartEditor2Skin.html",
		htParams : {
			bUseToolbar : true,				// 툴바 사용 여부 (true:사용/ false:사용하지 않음)
			bUseVerticalResizer : true,		// 입력창 크기 조절바 사용 여부 (true:사용/ false:사용하지 않음)
			bUseModeChanger : true,			// 모드 탭(Editor | HTML | TEXT) 사용 여부 (true:사용/ false:사용하지 않음)
			//aAdditionalFontList : aAdditionalFontSet,		// 추가 글꼴 목록
			fOnBeforeUnload : function(){
				//alert("완료!");
			}
		}, //boolean
		fOnAppLoad : function(){
			//예제 코드
			//oEditors.getById["ir1"].exec("PASTE_HTML", ["로딩이 완료된 후에 본문에 삽입되는 text입니다."]);
		},
		fCreator: "createSEditor2"
	});

	$(document).ready(function(){	
		$('#co_field').change(function(){
			 var val = $(this).val();
			 if(val == '') {
				 return false;
			 }
			$.ajax({
				type: "POST",
				url: "/admin/inc/lecturer_list.php",
				data: "co_field=" + val,
				dataType: "text",
				success: function(data) {
					
				//	alert("val set="+val);
					$('#co_mem_id').empty();
					$('#co_mem_id').append('<option value="">강사 선택</option>');
					$('#co_mem_id').append(data);
				},
				error: function(xhr, status, error) { alert(error); }
			});
		});
	
	
		$('#img_submit').click(function(){
			oEditors.getById["ir1"].exec("UPDATE_CONTENTS_FIELD", []);
			if($('#co_field').val() == '') {
				alert("강의분야 선택하세요");
				$('#co_field').focus();
				return false;
			}			
			if($('#co_title').val() == '') {
				alert("강의제목을 입력하세요");
				$('#co_title').focus();
				return false;
			}
			if($('#co_mem_id').val() == '') {
				alert("강의분야 선택하세요");
				$('#co_mem_id').focus();
				return false;
			}				
			
				
			if($('#co_start_date').val() == '') {
				alert("강의일정을 선택하세요");
				$('#co_start_date').focus();
				return false;
			}
			
			if($('#co_end_date').val() == '') {
				alert("강의일정을 입력하세요");
				$('#co_end_date').focus();
				return false;
			}

			var con	= $('#ir1').val();
			$('#co_contents').val(con);		
			if($('#co_contents').val() == '') {
				alert('내용을 입력하세요');
				return false;
			}				

			
			$('#base_form').attr('action','./proc/course_proc.php');
			$('#base_form').submit();
			return false;
		});					
	
	});
</script>
<script type="text/javascript">
	$(function() {
		callDatePick('co_start_date');
		callDatePick('co_end_date');
	});

	function callDatePick (id) {	
		if(id=="co_start_date"){
			var dates = $( "#" + id ).datepicker({
				prevText: '이전 달',
				nextText: '다음 달',
				monthNames: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
				monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
				dayNames: ['일','월','화','수','목','금','토'],
				dayNamesShort: ['일','월','화','수','목','금','토'],
				dayNamesMin: ['일','월','화','수','목','금','토'],
				dateFormat: 'yy-mm-dd',
				showMonthAfterYear: true,
				yearSuffix: '년',
				minDate	  : 0,
				onClose: function( selectedDate ) {    
					$("#co_end_date").val($("#co_start_date").val());
				}
			});
		}else{
			var dates = $( "#" + id ).datepicker({
				prevText: '이전 달',
				nextText: '다음 달',
				monthNames: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
				monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
				dayNames: ['일','월','화','수','목','금','토'],
				dayNamesShort: ['일','월','화','수','목','금','토'],
				dayNamesMin: ['일','월','화','수','목','금','토'],
				dateFormat: 'yy-mm-dd',
				showMonthAfterYear: true,
				yearSuffix: '년',
				minDate	  : 0
			});			
		}
	}
</script>