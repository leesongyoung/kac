<?php
	include_once("../../_init.php");
	
	include_once($GP->CLS."class.list.php");
	include_once($GP -> CLS."/class.member.php");	
	include_once($GP -> CLS."/class.course.php");	
	include_once($GP->CLS."class.button.php");
	$C_ListClass 	= new ListClass;
	$C_Member 	= new Member;
	$C_Button 		= new Button;
	$C_Course 		= new Course;
	
	ini_set("memory_limit" , -1);

	$field = $C_Course -> Course_field();
	
	
	$args = array();
	$args['show_row'] = 20;
	$args['pagetype'] = "admin";
	$args['coursetype'] = "satis";
	$data = "";
	$data = $C_Course->Course_List(array_merge($_GET,$_POST,$args));
	
	$data_list 		= $data['data'];
	$page_link 		= $data['page_info']['link'];
	$page_search 	= $data['page_info']['search'];
	$totalcount 	= $data['page_info']['total'];
	
	$totalpages 	= $data['page_info']['totalpages'];
	$nowPage 		= $data['page_info']['page'];
	$totalcount_l 	= number_format($totalcount,0);
	
	$data_list_cnt 	= count($data_list);
	
	include_once($GP -> INC_ADM_PATH."/head.php");
?>
<body>
<div class="Wrap"><!--// 전체를 감싸는 Wrap -->
		<? include_once($GP -> INC_ADM_PATH."/header.php"); ?>
		<div class="boxContentBody">
			<div class="boxSearch">

			<form name="base_form" id="base_form" method="GET">
			<ul>
				<li>
				<strong class="tit">분류</strong>
				<span>
					<select name="search_f">
						<option value="">:: 선택 ::</option>
						<?foreach($field as $name){?>
							<option value="<?=$name?>" <?if($_GET["search_f"] == $name){ echo "selected"; }?>><?=$name?></option>
						<?}?>
					</select>
				</span>
				</li>

				<li>
				<strong class="tit">만족도</strong>
				<span>
					<span><input type="text" name="search_satis1" value="<?=$_GET['search_satis1']?>" class="input_text" style="width:50px;" /></span>
					~
					<span><input type="text" name="search_satis2" value="<?=$_GET['search_satis2']?>" class="input_text" style="width:50px;" /></span>
				</span>
				</li>

				<li>
				<strong class="tit">향상도</strong>
				<span>
					<span><input type="text" name="search_improve1" value="<?=$_GET['search_improve1']?>" class="input_text" style="width:50px;" />%</span>
					~
					<span><input type="text" name="search_improve2" value="<?=$_GET['search_improve2']?>" class="input_text" style="width:50px;" />%</span>
				</span>
				</li>

				<li>
					<strong class="tit">강의날짜</strong>
					<span><input type="text" name="s_date" id="s_date" value="<?=$_GET['s_date']?>" class="input_text" size="13"></span>
					<span>~</span>
					<span><input type="text" name="e_date" id="e_date" value="<?=$_GET['e_date']?>" class="input_text" size="13" /></span>
				</li>
				
				<li>
					<strong class="tit">검색조건</strong>
					<span>
					<select name="search_key" id="search_key">
						<option value="">:: 선택 ::</option>
						<option value="co_mem_name" <? if($_GET['search_key'] == "co_mem_name"){ echo "selected";}?> >강사명</option>
						<option value="co_agency" <? if($_GET['search_key'] == "co_agency"){ echo "selected";}?> >강의처</option>
						<option value="co_title" <? if($_GET['search_key'] == "co_title"){ echo "selected";}?>>강의주제</option>
					</select>
					</span>
					<span><input type="text" name="search_content" id="search_content" value="<?=$_GET['search_content']?>" class="input_text" size="30" /></span>
					<span><button id="search_submit" class="btnSearch ">검색</button></span>
					<!--<span><input type="button" id="sel_email_btn" value="선택메일" /></span>
					<span><input type="button" id="gr_email_btn" value="그룹메일" /></span>
					<span><input type="button" id="pn_email_btn" value="개인메일" /></span>
                    -->
				</li>
			</ul>
			</form>
			</div>
		</div>	
        	
		<div id="BoardHead" class="boxBoardHead">				
				<div class="boxMemberBoard">
					<table>
						<colgroup>
							<col />
							<col />
							<col />
							<col />
							<col />
							<col />
							<col />
							<col />
							<col />
							<col />
							<col />
						</colgroup>
						<thead>
							<tr>
								<th>No</th>								
								<th>분류</th>								
								<th>성별</th>								
								<th>강사명</th>								
								<th>년도</th>								
								<th>날짜</th>								
								<th>시간</th>								
								<th>강의처</th>
								<th>강의주제</th>
								<th>만족도</th>
								<th>향상도</th>
							</tr>
						</thead>
						<tbody>
							<?
								$dummy = 1;
								for ($i = 0 ; $i < $data_list_cnt ; $i++) {
									$co_idx 		= $data_list[$i]['co_idx'];
									$co_title		= $data_list[$i]['co_title'];
									$co_field		= $data_list[$i]['co_field'];
									$co_mem_id		= $data_list[$i]['co_mem_id'];
									$co_mem_name	= $data_list[$i]['co_mem_name'];
									$co_start_date	= $data_list[$i]['co_start_date'];
									$co_end_date	= $data_list[$i]['co_end_date'];
									$co_start_date_time = strtotime($co_start_date);
									$co_end_date_time = strtotime($co_end_date);

									$co_agency	= $data_list[$i]['co_agency'];
									$co_title	= $data_list[$i]['co_title'];
									$co_satis	= $data_list[$i]['co_satis'];
									$co_improve	= $data_list[$i]['co_improve'];
									

									$week = date("w", $co_start_date_time);

									switch($week){
										case 0:
											$week = "일";
											break;

										case 1:
											$week = "월";
											break;

										case 2:
											$week = "화";
											break;

										case 3:
											$week = "수";
											break;

										case 4:
											$week = "목";
											break;

										case 5:
											$week = "금";
											break;

										case 6:
											$week = "토";
											break;
									}

									$member = $C_Member -> Mem_Info_ID($co_mem_id);
									$mb_sex = $member['mb_sex'];
									
								?>
										<tr>
											<td><?=$data['page_info']['start_num']--?></td>					
											<td><?=$co_field?></td>											
											<td><?=$mb_sex?></td>											
											<td><?=$co_mem_name?></td>											
											<td><?=date("Y", $co_start_date_time)?></td>
											<td><?=date("m월 d일 (".$week.")", $co_start_date_time)?></td>
											<td><?=date("H:i", $co_start_date_time)."~".date("H:i", $co_end_date_time)?></td>
											<td><?=$co_agency?></td>											
											<td><?=$co_title?></td>
											<td><?=$co_satis?></td>
											<td><?=($co_improve != '') ? $co_improve.'%' : ''?></td>
										</tr>
										<?
									$dummy++;
								}
								?>						
						</tbody>
					</table>
				</div>			
			</div>
			<ul class="boxBoardPaging">
				<?=$page_link?>
			</ul>
		</div>
		<? include_once($GP -> INC_ADM_PATH."/footer.php"); ?>
	</div>
</div><!-- 전체를 감싸는 Wrap //-->
</body>
</html>
