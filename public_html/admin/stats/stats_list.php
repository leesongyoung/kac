<?php
	include_once("../../_init.php");
	include_once($GP -> INC_ADM_PATH."/head.php");
	
	include_once($GP->CLS."class.list.php");
	include_once($GP -> CLS."/class.course.php");	
	include_once($GP->CLS."class.button.php");
	$C_ListClass 	= new ListClass;
	$C_Course 	= new Course;
	$C_Button 		= new Button;
	
	$args = array();
	
	if (!$_GET['s_date']) {
		$s_date = date("Y-m-d");		
	}else{
		$s_date = $_GET['s_date'];		
	}
	if (!$_GET['e_date']) {
		$e_date = date("Y-m-d");		
	}else{
		$e_date = $_GET['e_date'];		
	}


$satis = $C_Course -> Course_satis();
$cate = $C_Course -> Course_satis_cate();



	$args['show_row'] = 15;
	$args['pagetype'] = "admin";
	$data = "";
	$data = $C_Course->Course_List(array_merge($_GET,$_POST,$args));
	
	$rst = $C_Course->Course_New_Reply_Cnt();	
	$ncnt = $rst["cnt"];
	$data_list 		= $data['data'];
	$page_link 		= $data['page_info']['link'];
	$page_search 	= $data['page_info']['search'];
	$totalcount 	= $data['page_info']['total'];
	
	$totalpages 	= $data['page_info']['totalpages'];
	$nowPage 		= $data['page_info']['page'];
	$totalcount_l 	= number_format($totalcount,0);
	
	$data_list_cnt 	= count($data_list);
?>
<body>

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>

<div class="Wrap"><!--// 전체를 감싸는 Wrap -->
		<? include_once($GP -> INC_ADM_PATH."/header.php"); ?>
		<div class="boxContentBody">
			<div class="boxSearch">
			<!--? include_once($GP -> INC_ADM_PATH."/inc.mem_search.php"); ?-->										
			<form name="base_form" id="base_form" method="GET">
			<ul>
				
				<strong class="tit">연도별</strong>
				<span>
					<select name="year" id="year">
						<option value="2018">2018</option>
					</select>
				</span>
				
				<li>
				<strong class="tit">프로그램별</strong>
				<span>
					<select name="search_key" id="search_key">
						<option value="">:: 선택 ::</option>
						<?foreach($cate as $name){?>
							<option value="<?=$name?>" <?if($_GET["search_key"] == $name){ echo "selected"; }?>><?=$name?></option>
						<?}?>
					</select>
				</span>
				<span><button id="search_submit" class="btnSearch ">검색</button></span>
				</li>

			</ul>
			</form>
			</div>
		</div>	

		<div id="BoardHead" class="boxBoardHead">
			<canvas id="myChart"></canvas>
		</div>
		<? include_once($GP -> INC_ADM_PATH."/footer.php"); ?>
	</div>
</div><!-- 전체를 감싸는 Wrap //-->
</body>

<style>
canvas{
	width: 100% !important;
	max-width: 1024px;
	height: auto !important;
}

</style>
<script>
$(function(){
	var labels = [];

	for(var i=1; i<=12; i++){
		labels.push(i+"월");
	}

	var ctx = document.getElementById("myChart").getContext('2d');
	var myChart = new Chart(ctx, {
		type: 'line',
	   data: {
			labels: labels,
			datasets: [{
				label: '만족도',
				data: <?=$satis?>,
				fill: false,
				backgroundColor: ['rgba(66, 139, 202, 1)'],
				borderColor: ['rgba(66, 139, 202, 1)'],
				borderWidth: 1
			}]
		},
		options: {
			scales: {
				yAxes: [{
					ticks: {
						beginAtZero:true,
						max:5.0
					}
				}]
			},
			animation:{
				duration: 0
			}
		}
	});
});

</script>

</html>