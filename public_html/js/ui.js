var slider = [],
	co = [],
	fn = [],
	doc = $(document),win = $(window);
// document size type
co.getdoc = (function(body,viewport,size,winWidth){
	body =$('body');
	switch (body.css('z-index')){
		case '1' : if (co.device != 'pc'){co.device = 'pc';co.respond = true;co.respondView = true;}break;
		case '2' : if (co.device != 'tb'){co.device = 'tb';co.respond = true;co.respondView = true;}break;
		case '3' : if (co.device != 'mb'){co.device = 'mb';co.respond = true;co.respondView = true;}break;
	}
	viewport = document.getElementById('viewport');
	if (viewport){
		//size = parseInt(body.css('min-width'));
		winWidth = window.screen.width;
		if (co.device == 'mb'){
			viewport.setAttribute('content','user-scalable=no, width=320px');
		} else {
			viewport.setAttribute('content','width=device-width, initial-scale=1, user-scalable=no');
		}
	}
	$('#wrap').removeClass('hidden');
});

slider.item = [];
fn.sliderOption = function(el,custom,option){
	el = el.parent();
	var id = el.attr('id');
	!custom.speed && (custom.speed = 500);
	!custom.pause && (custom.pause = 6000);
	custom.pagerCustom && (custom.pagerCustom = $(custom.pagerCustom));
	option = {
		responsive: true,
		touchEnabled: true,
		oneToOneTouch :false,
		pager : false,
		controls : false,
		autoControls :false,
	};
	$.extend(option,custom);
	if (custom.buildPager)option.buildPager = function(sliderIndex){
		return eval(custom.buildPager)(el, sliderIndex);
	}
	option.onSlideBefore = function($slideElement, oldIndex, newIndex){
		custom.onSlideBefore && eval(custom.onSlideBefore)(el, $slideElement, oldIndex, newIndex)
		$slideElement.siblings('.active').removeClass('active');
		setTimeout(function(){
			$slideElement.addClass('active');
		});
		if (option.auto && !option.autoControls){
			slider.item[id].stopAuto();
			slider.item[id].startAuto();
		}
	}
	option.onSliderLoad = function(currentIndex){
		custom.onSliderLoad && eval(custom.onSliderLoad)(el, currentIndex);
		setTimeout(function(){
			el.find('.swiper').children().eq(0).addClass('active');
		});
	};
	return option;
};
co.currentOffset = function(){
	var eq,target,href,offset,index,
		win = $(window),
		interval = win.height() * .2,
		wrap = $('#wrap'),
		dp1 = $('#nav').find('.dp1'),
		headerOffset = (co.device == 'pc' ? 110 : parseInt(wrap.css('padding-top'))),
		scrollTop = win.scrollTop();
	index = 0;
	for (eq = 0; eq < dp1.length; eq++){
		href = dp1.eq(eq).children('a').attr('href');
		if (window.location.pathname == '/'){
			href = href.replace(/\//gi,'');
			target = $(href);
			if (target.length){
				offset = target.offset().top - 110;
				if (offset - interval < scrollTop){
					index = eq;
				}
			}
		}
	}
	if (scrollTop > 0){//$('#home').outerHeight() - $('#header').outerHeight()){
		wrap.addClass('fixed');
	} else {
		wrap.removeClass('fixed');
	}
	dp1.removeClass('active');
	index >= 0 && dp1.eq(index).addClass('active');
}
co.callLogin = function(){
	$('#signin').addClass('on');
};
co.textAutoSize = function(){
	$("textarea.autosize").each(function(eq,el){
		el = $(el);
		var cloneArea = el.next('.clone');
		if (!cloneArea.length){
			cloneArea = $('<div class="clone"/>');
			cloneArea.addClass(el.attr('class'));
			cloneArea.css({
				'position' : 'absolute',
				'height' : '0',
				'min-height' : '0',
				'visibility':'hidden',
			});
			el.after(cloneArea);
		}
		cloneArea.html(el.html());
		co.textResize(el);
		el.off('keydown keyup').on('keydown keyup', function () {
			co.textResize($(this));
		});
	});
};
co.textResize = function(el){
	var win = $(window),
		winHeight =  win.height(),
		scrollTop = win.scrollTop(),
		cloneArea = el.next();
	cloneArea.html(el.val());
	el.height(cloneArea.prop('scrollHeight') - (parseInt(el.css('padding-top')) * 2));
	if (scrollTop < cloneArea.offset().top - winHeight){
		$('html, body').scrollTop(cloneArea.offset().top - winHeight);
	}
};
co.interface = function(){
	$(window).off('hashchange');
	var wrap = $('#wrap'),
		header = $('#header'),
		nav = header.find('#nav'),
		aside = nav.find('.aside'),
		iLabel = $('.i-label'),
		cUrl = window.location;
	$('.go2top').on('click', function(){
		$('html, body').scrollTop(0);
		doc.trigger('click.selectBlur');
		return false;
	});
	$('.swiper').each(function(eq, el){
		el = $(el);
		var id = el.parent().attr('id');
		slider.item[id] = el.bxSlider(fn.sliderOption(el,el.data()));
	});
	iLabel.children('.i-placeholder').on('click',function(){
		$(this).next().trigger('focus');
	});
	iLabel.children('.i-text').on({
		'focus focusin' : function(){
			$(this).parent().children('.i-placeholder').hide();
		},
		'blur focusout' : function(input, placeholder){
			input = $(this),
			placeholder = $(this).parent().children('.i-placeholder');
			if (input.val().length > 0){
				placeholder.hide();
			} else {
				placeholder.show();
			}
		},
	});
	iLabel.each(function(eq,label){
		label = $(label);
		var input = label.children('.i-text'),
			placeholder = label.children('.i-placeholder');
		if (input.val().length > 0){
			placeholder.hide();
		} else {
			placeholder.show();
		}
	});
	nav.children('.trigger').on('click','a',function(){
		$('body').toggleClass('nav-active');
		return false;
	});
	nav.find('.panel').children('.btn-close').on('click',function(){
		$('body').removeClass('nav-active');
		return false;
	});
	nav.find('.dp1').on('click','a',function(){
		var offset, target = $(this),
			href = target.attr('href').replace(/\//gi,''),
			headerOffset = (co.device == 'pc' ? 110 : parseInt(wrap.css('padding-top')))
		if (window.location.pathname == '/'){
			offset = $(href).offset().top - headerOffset;
			cUrl.hash = href.replace('#','#/');
			$('html,body').animate({'scrollTop':offset},250);
			$('body').removeClass('nav-active');
			return false;
		}
	});
	aside.on('click','.trigger',function(){
		var me = $(this);
		if (aside.hasClass('on') || aside.find('.panel').hasClass('session-true')){
			aside.toggleClass('on');
		} else {
			co.callLogin();
		}
		return false;
	});
	aside.find('.btn-close').on('click',function(){
		aside.removeClass('on');
		return false;
	});
	$('#signin').on('click','.overlay',function(){
		$('#signin').removeClass('on');
	}).on('click','.btn-contact',function(){
		nav.find('[href="#contact"]').trigger('click');
		$('#signin').removeClass('on');
		return false;
	});
	nav.on('click','.btn-sign-teacher',function(){
		if (!$(this).hasClass('session-true')){
			co.callLogin();
			return false;
		} else {
			// 로그인 이후?
		}
	});
	co.currentOffset();
	if (cUrl.hash != ''){
		nav.find('a[href="'+cUrl.pathname+cUrl.hash+'"]').trigger('click');
	}
	setTimeout(function(){
		$('.ani-s').switchClass('ani-s','ani-e',1750);
	},750);
};
;(function($){
	co.init = function(){
		co.getdoc();
		co.interface();
		co.textAutoSize();
		var ieOldVer = $('#ie-version');
		if (ieOldVer.length){
			ieOldVer.on('click','.version-close',function(){
				ieOldVer.remove();
			});
		}
	};
	co.reszieEvent = 'orientationchange' in window ? 'orientationchange' : 'resize';
	co.resize = function(){
		co.getdoc();
		co.textAutoSize();
		$('body').removeClass('nav-active');
		$('#nav').find('.aside').removeClass('on');
	};
	co.scroll = function(){
		if (co.device == 'mb'){
			var scrollTop = $(window).scrollTop();
			var wrap = $('#wrap');
			if (scrollTop >= $('.hgroup').outerHeight()){
				wrap.addClass('fixed');
			} else {
				wrap.removeClass('fixed');
			}
		}
		co.currentOffset();
		if (fn.scroll) fn.scroll();
	};
	$(doc).on('ready',function(){
		co.init();
		if (fn.init) fn.init();
	});
	$(win).on(co.reszieEvent, function() {
		co.resize();
		if (fn.resize) fn.resize();
		co.respond = false;
	})
	.on('scroll',function(){
		co.scroll();
	});
}(jQuery));
Date.prototype.format = function(f) {
	if (!this.valueOf()) return ' ';
	var weekName = ['sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat'];
	var d = this;
	var h;
	return f.replace(/(yyyy|yy|MM|dd|E|hh|mm|ss|a\/p|w)/gi, function($1) {
		switch ($1) {
			case 'yyyy': return d.getFullYear();
			case 'yy': return (d.getFullYear() % 1000).zf(2);
			case 'MM': return (d.getMonth() + 1).zf(2);
			case 'dd': return d.getDate().zf(2);
			case 'E': return weekName[d.getDay()];
			case 'HH': return d.getHours().zf(2);
			case 'hh': return ((h = d.getHours() % 12) ? h : 12).zf(2);
			case 'mm': return d.getMinutes().zf(2);
			case 'ss': return d.getSeconds().zf(2);
			case 'a/p': return d.getHours() < 12 ? 'AM' : 'PM';
			case 'w': return (Math.floor((d.getDate() + (7 - d.getDay()) ) / 7) + 1);
			default: return $1;
		}
	});
};
String.prototype.string = function(len){var s = '', i = 0; while (i++ < len) { s += this; } return s;};
String.prototype.zf = function(len){return '0'.string(len - this.length) + this;};
Number.prototype.zf = function(len){return this.toString().zf(len);};
Array.prototype.remove=function(){for(var t,r,e=arguments,i=e.length;i&&this.length;)for(t=e[--i];-1!==(r=this.indexOf(t));)this.splice(r,1);return this};
if(typeof String.prototype.trim !== 'function') {
	String.prototype.trim = function() {
		return this.replace(/^\s+|\s+$/g, ''); 
	};
}