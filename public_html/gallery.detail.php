<?php 
include_once "./_init.php";
include_once "./inc/head.php"; ?>
</head>
<body>
<?php include_once "./inc/header.php"; ?>
	<div id="container" class="gallery">
		<div id="top">
			<p class="title">활동사진</p>
		</div>
		<div id="article">
			<div class="header bxsdw">
				<h3 class="title">활동사진</h3>
				<div class="location">
					<ul>
						<li class="home"><a href="/" >Home</a></li>
						<li class="current"><span>활동사진</span></li>
					</ul>
				</div>
			</div>
			<div class="section view bxsdw">
				<table class="view-infos">
					<colgroup>
						<col width="110px" />
						<col width="*" />
						<col width="110px" />
						<col width="*" />
					</colgroup>
					<tbody>
						<tr>
							<th class="subject">제목</th>
							<td class="subject" colspan="3"><strong>0000 제목</strong></td>
						</tr>
						<tr>
							<th>작성일</th>
							<td><span>2017.12.12</span></td>
							<th>작성자</th>
							<td><span>가나다</span></td>
						</tr>
						<tr>
							<th>첨부파일</th>
							<td class="file" colspan="3"><a href="#">XXXX.jpg</a></td>
						</tr>
					</tbody>
				</table>
				<div class="view-contents">
					<p>직업기초교육 사진입니다.
					<br />
					<br /></p>
					<img src="/public/images/sample.jpg" alt="" />
				</div>
				<div class="btn-group">
					<ul class="controls-util">
						<li><a href="#" class="btn"><span>Prev</span></a></li>
						<li><a href="#" class="btn"><span>Next</span></a></li>
					</ul>
					<ul class="local-util">
						<li><a href="/gallery.list.php" class="btn"><span>List</span></a></li>
					</ul>
				</div>
			</div>
			<form action="?" class="search-section">
				<select name="" id="" class="condition">
					<option value="">제목 + 내용</option>
				</select>
				<input type="search" class="i-search" />
				<button type="submit" class="btn-submit"><span>검색</span></button>
			</form>
		</div>
	</div>
<?php include_once "./inc/footer.php"; ?>
</body>
</html>