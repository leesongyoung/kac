<?php 
include_once("./_init.php");
include_once "./inc/head.php"; 
include_once "./inc/inc.login_check.php"; 
include_once($GP->CLS."class.list.php");
include_once($GP -> CLS."/class.course.php");
include_once($GP->CLS."class.button.php");
$C_ListClass 	= new ListClass;
$C_Course 	= new Course;
$C_Button 		= new Button;
$args = array();
$args['show_row'] = 5;
$args['co_mem_id'] = $_SESSION["suserid"];
$data = "";
$data = $C_Course->Course_List(array_merge($_GET,$_POST,$args));

$data_list 		= $data['data'];
$page_link 		= $data['page_info']['link'];
$page_search 	= $data['page_info']['search'];
$totalcount 	= $data['page_info']['total'];

$totalpages 	= $data['page_info']['totalpages'];
$nowPage 		= $data['page_info']['page'];
$totalcount_l 	= number_format($totalcount,0);
$data_list_cnt 	= count($data_list);
?>
</head>
<body>
<?php include_once "./inc/header.php"; ?>
	<div id="container" class="edu-schedule">
		<div id="top">
			<p class="title">강의일정</p>
		</div>
		<div id="article">
			<div class="header bxsdw">
				<h3 class="title">강의일정</h3>
				<div class="location">
					<ul>
						<li class="home"><a href="/" >Home</a></li>
						<li class="current"><span>강의일정</span></li>
					</ul>
				</div>
			</div>
			<div class="section teacher-info bxsdw">
				<div class="contain">
					<dl class="header">
						<dt class="teacher">
							<div class="picture"><img src="/public/images/empty-user.jpg" alt="" class="block"></div>
							<span class="name"><?=$Meber_name?></span>
						</dt>
						<dd class="rate">
							<i class="ip-icon-rate"></i>
							<span class="score"><?=$Meber_satis?></span>
							<span class="max">5.00</span>
						</dd>
					</dl>
					<div class="detail">
						<dl class="cate">
							<dt>강의분야</dt>
							<dd>
								<ul class="list">
                                	<? 
									$field_arr = explode(",",$Meber_field);
										foreach($field_arr as $val) { 
											echo '<li><a href="#">#'.$val.'</a></li>';
										}
									?>
								</ul>
							</dd>
						</dl>
						<dl class="rep">
							<dt>대표강의</dt>
							<dd>
								<ul class="list">
									<?php
										$course = "<li>";
										$mb_course = $Meber_course;
										$mb_course = str_replace("\n","</li><li>",$mb_course);
										$course .= $mb_course;
										echo $course;
									?>
								</ul>
							</dd>
						</dl>
					</div>
				</div>
			</div>
			<div class="section lectures-list bxsdw">
				<div class="header">
					<h3 class="text-ir">강의일정 목록 목차</h3>
					<ul class="list">
						<li class="no">No</li>
						<li class="date">강의일시</li>
						<li class="cate">분야</li>
						<li class="subject">제목</li>
						<li class="imp">향상도</li>
						<li class="sat">만족도</li>
					</ul>
				</div>
				<div class="body">
					<ul class="list" id="">
						 <?
								$dummy = 1;
								for ($i = 0 ; $i < $data_list_cnt ; $i++) {
									$co_idx 		= $data_list[$i]['co_idx'];
									$co_title		= $data_list[$i]['co_title'];
									$co_improve		= $data_list[$i]['co_improve'];
									$co_satis		= $data_list[$i]['co_satis'];
									$co_field		= $data_list[$i]['co_field'];
									$co_mem_id		= $data_list[$i]['co_mem_id'];
									$co_mem_name	= $data_list[$i]['co_mem_name'];
									$co_start_date	= date("Y.m.d H:i:s", strtotime($data_list[$i]['co_start_date']));
									$co_end_date	= date("Y.m.d H:i:s", strtotime($data_list[$i]['co_end_date']));
									//$co_satis = ($co_satis) ? $co_satis : "-";
									
									$nDate = date("Y-m-d H:i:s");
									$eDate = date("Y-m-d H:i:s", strtotime($data_list[$i]['co_start_date']));
									
									$datetime1 = date_create($nDate);
									$datetime2 = date_create($eDate);
									$interval = date_diff($datetime1, $datetime2);
									$new_icon = ($nDate < $eDate && $interval->days < 2) ? '<span class="next">NEXT</span>': "";
									//echo $nDate."//".$eDate;
									//$interval = date_diff($nDate, $eDate);
									$data = $C_Course->Course_Satis_Info($co_idx);
									if($data) {
										extract($data);
										if($co_satis) {
											$percent = 100 - ($before_satis / $co_satis * 100);
											$percent = round($percent,1)."%";
										}else{
											$co_satis = "-";
											$percent = "-";			
										}
									}			
									if($percent == "100%") $percent = "-";

									$comment_order='';
									$comment_order = $C_Course->Course_Comment_Order($co_idx);


									if($comment_order["cc_mem_level"] == 9){
										$comment ="<span class='success'>답변완료</span>";
									}else if($comment_order["cc_mem_level"] == 3){
										$comment ="<span class='wait'>답변대기</span>";
									}
								
								
								?>
						            <li><a href="schedule.detail.php?co_idx=<?=$co_idx?>" class="panel">
                                        <dl>
                                            <dt class="no">No</dt><dd class="no"><?=$totalcount--?></dd>
                                            <dt>강의일시</dt><dd class="date"><span class="nowrap"><?=$co_start_date?></span> <span class="nowrap"><?=$co_end_date?></span></dd>
                                            <dt>분야</dt><dd class="cate"><?=$co_field?></dd>
                                            <dt class="text-ir">제목</dt><dd class="subject">
                                                <span class="text"><?=$co_title?></span>
                                                <?=$new_icon?>
												<?=$comment?>
                                            </dd>
                                            <dt>향상도</dt><dd class="imp"><?=$co_improve?></span></dd>
                                            <dt>만족도</dt><dd class="sat"><?=$co_satis?></span></dd>
                                        </dl>
                                    </a></li>                                
										<?
										$dummy++;
									}
							?>		                    
					</ul>
				</div>
			</div>
			<a href="Javascript:void(0)" id="more" class="section lectures-more bxsdw">
				<i class="ip-icon-reload"></i>
				<span>Load More</span>
			</a>
		</div>
	</div>
<?php include_once "./inc/footer.php"; ?>
<input type="hidden" name="co_mem_id" id="co_mem_id" value="<?=$co_mem_id?>">
<input type="hidden" name="co_idx" id="co_idx" value="<?=$co_idx?>">
<script>
var cp_page = 1;
$('#more').click(function(){		
	var co_mem_id = $('#co_mem_id').val();
	var co_idx = $('#co_idx').val();
	$.ajax({
		type: "POST",
		url: "/bbs/action/schedule_ajax.php",
		data:{
		"cp_page" : cp_page,
		"co_mem_id" : co_mem_id,
		"co_idx" : co_idx
		},
		dataType: "text",
		beforeSend: function() {
			//$('#ajax_load').html('<img src="/images/ajax-loader.gif">');
		},
		success: function(data) {
			$(".lectures-list .body .list").append(data);
		//	alert(cp_page);
		//	data.hasClass('non-list') && $(".gallery-grid.list").addClass('empty') || $(".gallery-grid.list").removeClass('empty');
			cp_page++;
		},
		error: function(xhr, status, error) { alert(error); }
	});		
});	
</script>
</body>
</html> 