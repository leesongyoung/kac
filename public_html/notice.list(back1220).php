<?php 
include_once "./_init.php";
include_once "./inc/head.php"; 
?>
</head>
<body>
<?php include_once "./inc/header.php"; ?>
	<div id="container" class="notice">
		<div id="top">
			<p class="title">공지사항</p>
		</div>
		<div id="article">
			<div class="header bxsdw">
				<h3 class="title">공지사항</h3>
				<div class="location">
					<ul>
						<li class="home"><a href="/" >Home</a></li>
						<li class="current"><span>공지사항</span></li>
					</ul>
				</div>
			</div>
			<div class="section">
				<ul class="board-list bxsdw">
					<li><a href="/notice.detail.php" class="panel">
						<dl>
							<dt class="text-ir">제목</dt><dd class="subject">소식, 뉴스, 업계의 동정 등을 안내 </dd>
							<dt class="text-ir">내용</dt><dd class="contents">소식, 뉴스, 업계의 동정 등을 안내 소식, 뉴스, 업계의 동정 등을 안내 소식, 뉴스, 업계의... </dd>
							<dt class="text-ir">작성일</dt><dd class="date">2017.10.15</dd>
							<dt class="text-ir">작성자</dt><dd class="name">관리자</dd>
						</dl>
					</a></li>
					<li><a href="/notice.detail.php" class="panel">
						<dl>
							<dt class="text-ir">제목</dt><dd class="subject">소식, 뉴스, 업계의 동정 등을 안내 </dd>
							<dt class="text-ir">내용</dt><dd class="contents">소식, 뉴스, 업계의 동정 등을 안내 소식, 뉴스, 업계의 동정 등을 안내 소식, 뉴스, 업계의... </dd>
							<dt class="text-ir">작성일</dt><dd class="date">2017.10.15</dd>
							<dt class="text-ir">작성자</dt><dd class="name">관리자</dd>
						</dl>
					</a></li>
					<li><a href="/notice.detail.php" class="panel">
						<dl>
							<dt class="text-ir">제목</dt><dd class="subject">소식, 뉴스, 업계의 동정 등을 안내 </dd>
							<dt class="text-ir">내용</dt><dd class="contents">소식, 뉴스, 업계의 동정 등을 안내 소식, 뉴스, 업계의 동정 등을 안내 소식, 뉴스, 업계의... </dd>
							<dt class="text-ir">작성일</dt><dd class="date">2017.10.15</dd>
							<dt class="text-ir">작성자</dt><dd class="name">관리자</dd>
						</dl>
					</a></li>
					<li><a href="/notice.detail.php" class="panel">
						<dl>
							<dt class="text-ir">제목</dt><dd class="subject">소식, 뉴스, 업계의 동정 등을 안내 </dd>
							<dt class="text-ir">내용</dt><dd class="contents">소식, 뉴스, 업계의 동정 등을 안내 소식, 뉴스, 업계의 동정 등을 안내 소식, 뉴스, 업계의... </dd>
							<dt class="text-ir">작성일</dt><dd class="date">2017.10.15</dd>
							<dt class="text-ir">작성자</dt><dd class="name">관리자</dd>
						</dl>
					</a></li>
				</ul>
				<div class="pagination">
					<a href="#" class="btn controls prev">
						<i class="ip-icon-page-prev"></i>
						<span class="text-ir">이전페이지</span>
					</a>
					<strong class="btn current">1</strong>
					<a href="#" class="btn">2</a>
					<a href="#" class="btn">3</a>
					<a href="#" class="btn">4</a>
					<a href="#" class="btn">5</a>
					<a href="#" class="btn controls next">
						<i class="ip-icon-page-next"></i>
						<span class="text-ir">다음페이지</span>
					</a>
				</div>
			</div>
			<form action="?" class="search-section">
				<select name="" id="" class="condition">
					<option value="">제목 + 내용</option>
				</select>
				<input type="search" class="i-search" />
				<button type="submit" class="btn-submit"><span>검색</span></button>
			</form>
		</div>
	</div>
<?php include_once "./inc/footer.php"; ?>
</body>
</html>