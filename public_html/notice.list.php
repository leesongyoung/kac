<?php 
include_once "./_init.php";
include_once "./inc/head.php"; 
$index_page = "notice.list.php";
$query_page = "query.php";
if(!$jb_code) $jb_code="10";
if($check_level < $db_config_data['jba_read_level']) {
	$C_Func->put_msg_and_back("해당게시판의 읽기권한이 없습니다.");
	die;
}
?>
</head>
<body>
<?php include_once "./inc/header.php"; ?>
	<div id="container" class="notice">
		<div id="top">
			<p class="title">공지사항</p>
		</div>
		<div id="article">
			<div class="header bxsdw">
				<h3 class="title">공지사항</h3>
				<div class="location">
					<ul>
						<li class="home"><a href="/" >Home</a></li>
						<li class="current"><span>공지사항</span></li>
					</ul>
				</div>
			</div>
            <?php include $GP -> INC_PATH ."/board_inc.php"; ?>
		</div>
	</div>
<?php include_once "./inc/footer.php"; ?>
</body>
</html>