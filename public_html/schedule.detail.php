<?php 
include_once("./_init.php");
include_once "./inc/head.php"; 
include_once "./inc/inc.login_check.php"; 
include_once($GP -> CLS."/class.course.php");
include_once($GP -> CLS."/class.member.php");


$C_Member 	= new Member;
$C_Course 	= new Course;	

$co_idx = $_GET['co_idx'];
$data = $C_Course->Course_Satis_Info($co_idx);
if($data) {
	extract($data);
	
	if($co_satis) {
		$percent = 100 - ($before_satis / $co_satis * 100);
		$percent = round($percent,2)."%";
	}else{
		$co_satis = "-";
		$percent = "-";			
	}
	$co_contents	= nl2Br($C_Func->dec_contents_view($co_contents));				
}

$cmtdata = $C_Course->Course_Comment_List($co_idx);
?>
</head>
<body>
<?php include_once "./inc/header.php"; ?>
	<div id="container" class="edu-schedule">
		<div id="top">
			<p class="title">강의일정</p>
		</div>
		<div id="article">
			<div class="header bxsdw">
				<h3 class="title">강의일정</h3>
				<div class="location">
					<ul>
						<li class="home"><a href="/" >Home</a></li>
						<li class="current"><span>강의일정</span></li>
					</ul>
				</div>
			</div>
			<div class="section view bxsdw">
				<table class="view-infos">
					<colgroup>
						<col width="110px" />
						<col width="*" />
						<col width="110px" />
						<col width="*" />
						<col width="110px" />
						<col width="*" />
					</colgroup>
					<tbody>
						<tr>
							<th class="subject">강의명</th>
							<td class="subject" colspan="5"><strong><?=$co_title?></strong></td>
						</tr>
						<tr>
							<th>강의분야</th>
							<td class="cate">
								<ul class="list">
									<li><a href="#">#<?=$co_field?></a></li>
								</ul>
							</td>
							<th class="state">향상도</th>
							<td class="state"><strong class="point"><?=$percent?></strong></td>
							<th class="state">만족도</th>
							<td class="state"><strong class="point"><?=$co_satis?></strong></td>
						</tr>
						<tr>
							<th>지역</th>
							<td><span><?=$co_area?></span></td>
							<th>대상기관</th>
							<td><span><?=$co_agency?></span></td>
							<th>대상</th>
							<td><span><?=$co_target?></span></td>
						</tr>
						<tr>
							<th>강의일시</th>
							<td><span><span class="nowrap"><?=date("Y.m.d H:i:s", strtotime($co_start_date));?></span> <span class="nowrap"><?=date("Y.m.d H:i:s", strtotime($co_end_date));?></span></span></td>
							<th>강의장소</th>
							<td colspan="3"><span><?=$co_place?></span></td>
						</tr>
						<tr>
							<th>참고사항</th>
							<td colspan="5"><?=$co_contents?></td>
						</tr>
					</tbody>
				</table>
				<div class="view-contents">
					<div class="talk-panel">
						<ul class="display">
							 <? for($i=0; $i<count($cmtdata); $i++) {
                            $cc_idx 	= $cmtdata[$i]['cc_idx'];
                            $mem_id		= $cmtdata[$i]['cc_mem_id'];
                            $mem_name	= $cmtdata[$i]['cc_mem_name'];
                            $mem_level	= $cmtdata[$i]['cc_mem_level'];							
                            $contents	= $cmtdata[$i]['cc_contents'];
                            //등록일
                            $cc_reg_date 				= date("Y.m.d H:i:s", strtotime($cmtdata[$i]['cc_reg_date']));	
                            //내용 (HTML TAG제한)
                          //  $contents = nl2br(strip_tags($contents, '<br>'));
					
							$cls = ($mem_level < 9) ? "teacher" : "manager";
                        ?>
							<li class="message">
								<dl class="panel <?=$cls?>">
									<dt>
										<strong class="name"><?=$mem_name?></strong>
										<small class="date"><?=$cc_reg_date?></small>
									</dt>
									<dd class="comment"><?=$contents?></dd>
								</dl>
							</li>                        
                        
                        <? } ?>
						</ul>
						<form class="send">
                             <input type="hidden" id="co_idx" value="<?=$co_idx?>">
							<textarea name="cc_comments" class="i-text autosize" id="cc_comments"></textarea>
							<button type="button" class="btn-submit" id="comment_submit">
								<i class="ip-icon-message-send"></i>
								<span class="text-ir">전송</span>
							</button>
						</form>
					</div>
				</div>
				<div class="btn-group">
					<!--ul class="controls-util">
						<li><a href="#" class="btn"><span>Prev</span></a></li>
						<li><a href="#" class="btn"><span>Next</span></a></li>
					</ul-->
					<ul class="local-util">
						<li><a href="/schedule.list.php" class="btn"><span>List</span></a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
<?php include_once "./inc/footer.php"; ?>
</body>
</html> 
<script>
	$("#img_list").click(function(){
		location.href = "course_list.php";	
	});
	$("#comment_submit").click(function(){
		if($("#cc_comments").val() == "") {
			alert("내용을 입력하세요");
			$("#cc_comments").focus();
			return false;	
		}
		var co_idx = $("#co_idx").val();
		var comments = $("#cc_comments").val();  
		$.ajax({
			type: "POST",
			url: "/inc/comment_update.php",
			data: "comments=" + comments + "&co_idx=" + co_idx,
			dataType: "text",
			success: function(data) {
				console.log(data);
				if(data == "true") {
					//alert("수정되었습니다.");	
					location.reload();
				}else{
					alert("오류가 발생했습니다. 관리자에게 문의하세요");						
				}
				parent.modalclose();
			},
			error: function(xhr, status, error) { alert(error); }
		});		
	});
</script>