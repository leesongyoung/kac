<?php 
include_once "./_init.php";
include_once "./inc/head.php"; 
include_once "./inc/inc.login_check.php"; 
$index_page = "lesson-file.php";
$query_page = "query.php";
if(!$jb_code) $jb_code="40";
?>
</head>
<body>
<?php include_once "./inc/header.php"; ?>
	<div id="container" class="lesson-file">
		<div id="top">
			<p class="title">강의교안/교재</p>
		</div>
		<div id="article">
			<div class="header bxsdw">
				<h3 class="title">강의교안/교재</h3>
				<div class="location">
					<ul>
						<li class="home"><a href="/" >Home</a></li>
						<li class="current"><span>강의교안/교재</span></li>
					</ul>
				</div>
			</div>
			<?php include $GP -> INC_PATH ."/board_inc.php"; ?>
		</div>
	</div>
<?php include_once "./inc/footer.php"; ?>
</body>
</html>