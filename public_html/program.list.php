<?php 
include_once "./_init.php";
include_once "./inc/head.php"; 
$index_page = "program.list.php";
$query_page = "query.php";
if(!$jb_code) $jb_code="20";

?>
</head>
<body>
<?php include_once "./inc/header.php"; ?>
	<div id="container" class="edu-program">
		<div id="top">
			<p class="title">교육프로그램</p>
		</div>
		<div id="article">
			<div class="header bxsdw">
				<h3 class="title">교육프로그램</h3>
				<div class="location">
					<ul>
						<li class="home"><a href="/" >Home</a></li>
						<li class="current"><span>교육프로그램</span></li>
					</ul>
				</div>
			</div>
			<?php include $GP -> INC_PATH ."/board_inc.php"; ?>
		</div>
	</div>
<?php include_once "./inc/footer.php"; ?>
</body>
</html>