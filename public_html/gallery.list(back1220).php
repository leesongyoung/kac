<?php 
include_once "./_init.php";
include_once "./inc/head.php"; 
$index_page = "gallery.list.php";
$query_page = "query.php";
if(!$jb_code) $jb_code="30";
?>
</head>
<body>
<?php include_once "./inc/header.php"; ?>
	<div id="container" class="gallery">
		<div id="top">
			<p class="title">활동사진</p>
		</div>
		<div id="article">
			<div class="header bxsdw">
				<h3 class="title">활동사진</h3>
				<div class="location">
					<ul>
						<li class="home"><a href="/" >Home</a></li>
						<li class="current"><span>활동사진</span></li>
					</ul>
				</div>
			</div>
			<div class="section">
				<ul class="gallery-list">
					<li><a href="/gallery.detail.php" class="panel bxsdw">
						<div class="picture"><img src="/public/images/img-photo-01.jpg" alt="" class="block"></div>
						<dl class="post">
							<dt class="text-ir">제목</dt><dd class="subject"><strong>0000 활동사진</strong></dd>
							<dt class="text-ir">날짜</dt><dd>2017.12.12</dd>
						</dl>
					</a></li>
					<li><a href="/gallery.detail.php" class="panel bxsdw">
						<div class="picture"><img src="/public/images/img-photo-02.jpg" alt="" class="block"></div>
						<dl class="post">
							<dt class="text-ir">제목</dt><dd class="subject"><strong>0000 활동사진</strong></dd>
							<dt class="text-ir">날짜</dt><dd>2017.12.12</dd>
						</dl>
					</a></li>
					<li><a href="/gallery.detail.php" class="panel bxsdw">
						<div class="picture"><img src="/public/images/img-photo-03.jpg" alt="" class="block"></div>
						<dl class="post">
							<dt class="text-ir">제목</dt><dd class="subject"><strong>0000 활동사진</strong></dd>
							<dt class="text-ir">날짜</dt><dd>2017.12.12</dd>
						</dl>
					</a></li>
					<li><a href="/gallery.detail.php" class="panel bxsdw">
						<div class="picture"><img src="/public/images/img-photo-02.jpg" alt="" class="block"></div>
						<dl class="post">
							<dt class="text-ir">제목</dt><dd class="subject"><strong>0000 활동사진</strong></dd>
							<dt class="text-ir">날짜</dt><dd>2017.12.12</dd>
						</dl>
					</a></li>
					<li><a href="/gallery.detail.php" class="panel bxsdw">
						<div class="picture"><img src="/public/images/img-photo-03.jpg" alt="" class="block"></div>
						<dl class="post">
							<dt class="text-ir">제목</dt><dd class="subject"><strong>0000 활동사진</strong></dd>
							<dt class="text-ir">날짜</dt><dd>2017.12.12</dd>
						</dl>
					</a></li>
					<li><a href="/gallery.detail.php" class="panel bxsdw">
						<div class="picture"><img src="/public/images/img-photo-01.jpg" alt="" class="block"></div>
						<dl class="post">
							<dt class="text-ir">제목</dt><dd class="subject"><strong>0000 활동사진</strong></dd>
							<dt class="text-ir">날짜</dt><dd>2017.12.12</dd>
						</dl>
					</a></li>
				</ul>
				<div class="pagination">
					<a href="#" class="btn controls prev">
						<i class="ip-icon-page-prev"></i>
						<span class="text-ir">이전페이지</span>
					</a>
					<strong class="btn current">1</strong>
					<a href="#" class="btn">2</a>
					<a href="#" class="btn">3</a>
					<a href="#" class="btn">4</a>
					<a href="#" class="btn">5</a>
					<a href="#" class="btn controls next">
						<i class="ip-icon-page-next"></i>
						<span class="text-ir">다음페이지</span>
					</a>
				</div>
			</div>
			<form action="?" class="search-section">
				<select name="" id="" class="condition">
					<option value="">제목 + 내용</option>
				</select>
				<input type="search" class="i-search" />
				<button type="submit" class="btn-submit"><span>검색</span></button>
			</form>
		</div>
	</div>
<?php include_once "./inc/footer.php"; ?>
</body>
</html>