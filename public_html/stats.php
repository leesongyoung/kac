<?php 
include_once "./_init.php";
include_once "./inc/head.php"; 
include_once "./inc/inc.login_check.php"; 

include_once($GP -> CLS."/class.course.php");

$index_page = "stats.php";

$C_Course 	= new Course;

$_GET['uid'] = $_SESSION["suserid"];

$satis = $C_Course -> Course_satis();


?>
</head>
<body>
<?php include_once "./inc/header.php"; ?>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>

	<div id="container" class="lesson-file">
		<div id="top">
			<p class="title">강의 만족도 통계</p>
		</div>
		<div id="article">
			<div class="header bxsdw">
				<h3 class="title">강의 만족도 통계</h3>
				<div class="location">
					<ul>
						<li class="home"><a href="/" >Home</a></li>
						<li class="current"><span>강의 만족도 통계</span></li>
					</ul>
				</div>
			</div>
			<div class="section">
				<div class="lessonfile-list bxsdw">
					<canvas id="myChart"></canvas>
				</div>
			</div>
		</div>
	</div>

<style>
canvas{
	width: 100% !important;
	max-width: 1024px;
	height: auto !important;
}

</style>
<script>
$(function(){
	var labels = [];

	for(var i=1; i<=12; i++){
		labels.push(i+"월");
	}

	var ctx = document.getElementById("myChart").getContext('2d');
	var myChart = new Chart(ctx, {
		type: 'line',
	   data: {
			labels: labels,
			datasets: [{
				label: '만족도',
				data: <?=$satis?>,
				fill: false,
				backgroundColor: ['rgba(66, 139, 202, 1)'],
				borderColor: ['rgba(66, 139, 202, 1)'],
				borderWidth: 1
			}]
		},
		options: {
			scales: {
				yAxes: [{
					ticks: {
						beginAtZero:true
					}
				}]
			},
			animation:{
				duration: 0
			}
		}
	});
});

</script>

<?php include_once "./inc/footer.php"; ?>
</body>
</html>