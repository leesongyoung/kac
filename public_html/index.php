<?php 
include_once "./_init.php";
include_once "./inc/head.php"; 
include_once "./inc/main.proc.php"; 
//print_r($_SESSION);
$M_Notice = Main_Notice("10");
$M_Edu 	  = Main_Notice("20");
$M_Photo  = Main_Notice("30");
?>
</head>
<body>
<?php include_once "./inc/header.php"; ?>
	<div id="container">
		<div id="home">
			<div class="panel">
				<div class="background ani-s"></div>
				<div class="contain type-01">
					<small class="tag">Korea Accreditation for Career</small>
					<strong class="highlight">교육을 바꿉니다. <br />세상을 바꿉니다.</strong>
					<span class="text">세상을 바꾸는 교육, <br />한국능력개발인증원</span>
				</div>
			</div>
		</div>
		<div id="business">
			<div class="contain">
				<ul class="list">
					<li><div class="panel bxsdw col">
						<div class="picture"><img src="/public/images/img-business-01.jpg" alt="" class="block" /></div>
						<dl class="post">
							<dt>
								<small class="tag">Instructor</small>
								<span>교육컨텐츠 개발/기획</span>
							</dt>
							<dd>100명의 학습자에게 1명의 교수자가 있다면, 그건 학습자가 아닌 교수자 중심의 수업이 됩니다.</dd>
						</dl>
					</div></li>
					<li><div class="panel bxsdw col">
						<div class="picture"><img src="/public/images/img-business-02.jpg" alt="" class="block" /></div>
						<dl class="post">
							<dt>
								<small class="tag">Platform</small>
								<span>온라인 플랫폼</span>
							</dt>
							<dd>맞춤, 경험, 실용으로 설계되어 학습자에게 즉각적인 효능감을 주고 현실적 어려움을 헤쳐나갈 수 있는 용기와 지혜가 됩니다.</dd>
						</dl>
					</div></li>
					<li><div class="panel bxsdw col">
						<div class="picture"><img src="/public/images/img-business-03.jpg" alt="" class="block" /></div>
						<dl class="post">
							<dt>
								<small class="tag">Publication</small>
								<span>출판사업</span>
							</dt>
							<dd>교육 메뉴얼을 엄수하여 각 클래스의 표준화된 교육, 측정가능한 교육, 품질개량이 가능한 교육을 실천하고 있습니다.</dd>
						</dl>
					</div></li>
					<li class="full"><div class="panel bxsdw greetings">
						<h2 class="title">인사말</h2>
						<div class="body">
							<p>1명의 학습자에게는 1명의 교수자로 학습자 중심의 교육이 됩니다. <br />100명의 학습자에게 1명의 교수자가 있다면, 그건 학습자가 아닌 교수자 중심의 수업이 됩니다. <br />다수의 학습자 중심의 수업을 진행하기 위해서는 클래스별 교육의 표준화가 전제되어야 하고, 한국능력개발인증원(협) 2015년 창립 이후 이러한 노력을 지속적으로 해왔습니다.</p>
							<p>교육은 학습과정이 아닌 학습 후 그 결실이 나타나기에 교육종료 시 평가는 클래스별로 표준화된 내용이어야 교육품질을 측정할 수 있습니다. <br />한국능력개발인증원(협) 교육의 품질에 집착하는 것은 지속적인 개선을 하겠단 다짐입니다.</p>
						</div>
						<div class="quote">
							<i class="ip-icon-greetings-quote"></i>
							<p class="comment">‘교육에는 마침표가 없다.’는 한국능력개발인증원(협)의 약속에 많은 응원과 지지를 부탁드립니다.</p>
							<p class="source">한국능력개발인증원(협) 임직원 일동</p>
						</div>
					</div></li>
				</ul>
			</div>
		</div>
		<div id="intro">
			<div class="contain bxsdw">
				<h2 class="title">조합소개</h2>
				<ul class="list">
					<li class="first">
						<i class="icon-intro-dotted"></i>
						<strong>학습자 <br class="mb-hide" />인식변화</strong>
					</li>
					<li>
						<i class="icon-intro-dotted"></i>
						<strong>수요자 맞춤형 교육 <br />경험/실용 중심설계</strong>
					</li>
					<li class="last">
						<i class="icon-intro-dotted"></i>
						<strong>교육품질 개선, <br class="mb-hide" />표준화</strong>
					</li>
				</ul>
				<dl class="explain">
					<dt>사회문제를 해결하는 교육</dt>
					<dd>한국능력개발인증원(협)은 진로, 취업, 창업, 기업 각 분야의 교육전문가들이 학습자들의 의식을 변화시켜 우리사회가 지속 발전하도록 사회문제해결형 수업모델을 구현하는 교육협동조합입니다.사회문제해결형 수업모델은 맞춤, 경험, 실용으로 설계되어 학습자에게 즉각적인 효능감을 주고 현실적 어려움을 헤쳐나갈 수 있는 용기와 지혜가 됩니다.더불어 고도로 훈련된 다수의 강사와 컨설턴트가 교육 메뉴얼을 엄수하여 각 클래스의 표준화된 교육, 측정가능한 교육, 품질개량이 가능한 교육을 실천하고 있습니다.</dd>
				</dl>
			</div>
		</div>
		<div id="organization">
			<div class="contain">
				<h2 class="title">조직도</h2>
				<div class="tree-list">
					<ul class="dp1 one">
						<li>
							<strong>조합원총회</strong>
							<ul class="dp2 one">
								<li>
									<strong>이사회</strong>
									<ul class="dp3 one">
									<li>
										<strong>이사장</strong>
										<ul class="dp4 side">
											<li class="side"><strong>감사</strong></li>
											<li>
												<strong>상임이사</strong>
												<ul class="dp5">
													<li class="first"><strong>경영전략팀</strong></li>
													<li><strong>컨텐츠기획팀</strong></li>
													<li><strong>취업교육팀</strong></li>
													<li><strong>진로교육팀</strong></li>
													<li><strong>창업교육팀</strong></li>
													<li class="last"><strong>기업교육팀</strong></li>
												</ul>
											</li>
										</ul>
									</li>
								</ul>
								</li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div id="program">
			<div class="contain">
				<h2 class="title">교육프로그램</h2>
				<div class="body">
					<ul class="slider list">
						<?=$M_Edu?>
						<li class="more"><a href="/program.list.php">
							<span class="box">
								<span class="row">
									<span class="col">
										<i class="icon-program-more"></i>
										<span>더보기</span>
									</span>
								</span>
							</span>
						</a></li>
					</ul>
				</div>
			</div>
		</div>
		<div id="photo">
			<div class="contain">
				<h2 class="title">활동사진</h2>
				<ul class="slider list">
					<?=$M_Photo?>
					<li class="more"><a href="/gallery.list.php">
						<span class="box">
							<span class="row">
								<span class="col">
									<i class="icon-program-more"></i>
									<span>더보기</span>
								</span>
							</span>
						</span>
					</a></li>
				</ul>
				<a href="/gallery.list.php" class="btn-more">
					<i class="icon-photo-more"></i>
					<span class="text-ir">더보기</span>
				</a>
			</div>
		</div>
		<div id="notice">
			<div class="contain">
				<div class="background"></div>
				<div class="body">
					<div class="header">
						<h2 class="title bxsdw">
							<span class="row"><span class="col">공지사항</span></span>
						</h2>
					</div>
					<ul class="list">
						<?=$M_Notice?>
					</ul>
					<div class="footer">
						<a href="/notice.list.php" class="btn-more bxsdw">
							<span class="row">
								<span class="col">
									<i class="icon-notice-more"></i>
									<span>더보기</span>
								</span>
							</span>
						</a>
					</div>
				</div>
			</div>
		</div>
		<div id="contact">
			<div class="contain bxsdw">
				<div class="header">
					<h2 class="title">Contact</h2>
					<p class="explain">조합에 대한 문의, 제안, 가입방법 등 <br />어떠한 주제도 환영합니다.</p>
				</div>
				<div class="body">
					<div class="location">
						<dl class="info">
							<dt class="text-ir">이메일</dt><dd class="mail"><a href="mailto:hellokac@gmail.com">hellokac@gmail.com</a></dd>
							<dt class="text-ir">주소</dt><dd class="address">부산 해운대구 <br class="mb-hide" />센텀동로 45, 6층</dd>
						</dl>
						<div class="wrap-map">
							<div id="daumRoughmapContainer1511917555847" class="root_daum_roughmap root_daum_roughmap_landing"></div>
						</div>
					</div>
					<form id="contact_frm" class="entry">
						<dl>
							<dt>성명</dt>
							<dd><label class="i-label">
								<span class="i-placeholder">성명을 입력해 주세요</span>
								<input type="text" id="c_name" class="i-text" name="c_name">
							</label></dd>
							<dt>이메일</dt>
							<dd><label class="i-label">
								<span class="i-placeholder">이메일을 입력해 주세요</span>
								<input type="email" id="c_email" class="i-text" name="c_email">
							</label></dd>
							<dt>연락처</dt>
							<dd><label class="i-label">
								<span class="i-placeholder">숫자만 입력</span>
								<input type="tel" id="c_phone" class="i-text" name="c_phone">
							</label></dd>
							<dt>제목</dt>
							<dd><label class="i-label">
								<span class="i-placeholder">제목을 입력하여 주십시오.</span>
								<input type="text" id="c_title" class="i-text" name="c_title">
							</label></dd>
							<dt>내용</dt>
							<dd><textarea id="c_content"  name="c_content" class="i-text"></textarea></dd>
						</dl>
						<button type="button" id="c_send" class="btn-submit">보내기</button>
					</form>
				</div>
			</div>
		</div>
	</div>
	<?
		$mobile_agent = '/(iPod|iPhone|Android|BlackBerry|SymbianOS|SCH-M\d+|Opera Mini|Windows CE|Nokia|SonyEricsson|webOS|PalmOS)/';

		// preg_match() 함수를 이용해 모바일 기기로 접속하였는지 확인
		if(preg_match($mobile_agent, $_SERVER['HTTP_USER_AGENT'])) {
			 include $_SERVER[DOCUMENT_ROOT]."/popup/popup_layer.php";
		}else{
			 include $_SERVER[DOCUMENT_ROOT]."/popup/popup_script.php";
		}
	?>
<!--[if gt IE 8]><!-->
<script charset="UTF-8" class="daum_roughmap_loader_script" src="http://dmaps.daum.net/map_js_init/roughmapLoader.js"></script>
<!--<![endif]-->
<script charset="UTF-8">
$("#c_send").click(function(){

	if($('#c_content').val() == '')	{
		alert('내용을 입력하세요');
		$('#c_content').focus();
		return false;
	}

	var formData = $("#contact_frm").serialize();
		$.ajax({
			type: "POST",
			url: "/inc/contact.mail.send.php",
			data: formData,
			dataType: "text",
			success: function(msg) {
		//		console.log(msg);
				if($.trim(msg) == "true") {
					alert("문의가 등록되었습니다. 감사합니다.");
					location.reload();
					return false;
				}else{
					alert(msg);
					return;
				}				
			},
			error: function(xhr, status, error) { alert(error); }
		});
});
slider.programOption = function(){
	var slideMargin = 30,
		slideItem = co.device == 'mb' ? 1 : 2,
		slideWidth = 800;
	var option = {
		infiniteLoop : false,
		responsive: true,
		touchEnabled: true,
		oneToOneTouch :false,
		pager : true,
		controls : true,
		autoControls :false,
		slideMargin : slideMargin,
		minSlides : slideItem,
		maxSlides : slideItem,
		moveSlides : slideItem,
		slideWidth : slideWidth,
	};
	return option;
};
slider.photoOption = function(){
	var slideMargin = 0,
		slideItem = co.device == 'mb' ? 1 : 3,
		slideWidth = 800;
	var option = {
		infiniteLoop : false,
		responsive: true,
		touchEnabled: true,
		oneToOneTouch :false,
		pager : true,
		controls : true,
		autoControls :false,
		slideMargin : slideMargin,
		minSlides : slideItem,
		maxSlides : slideItem,
		moveSlides : slideItem,
		slideWidth : slideWidth,
	};
	return option;
};
slider.photoSet = function(){
	if (slider.item["photo"]){
		slider.item["photo"].destroySlider();
		if (co.device == "mb"){
			slider.item["photo"].bxSlider(slider.photoOption());
		}
	} else if (co.device == "mb"){
		slider.item["photo"] = $('#photo').find('.slider').bxSlider(slider.photoOption());
	}
}
fn.init = function(){
	slider.item["program"] = $('#program').find('.slider').bxSlider(slider.programOption());
	if (co.device == "mb"){
		slider.item["photo"] = $('#photo').find('.slider').bxSlider(slider.photoOption());
	}
	if (window.daum){
		fn.rtime;
		fn.timeout = false;
		fn.delta = 200;
		fn.locationMap = new daum.roughmap.Lander({
			"timestamp" : "1511917555847",
			"key" : "kod8"
		});
		fn.locationMapReload = function(){
			if (new Date() - fn.rtime < fn.delta) {
				setTimeout(fn.locationMapReload, fn.delta);
			} else {
				$('.wrap-map').find('.map').empty();
				fn.timeout = false;
				fn.locationMap.renderMap();
				fn.locationMap.setMarker();
			}
		};
		$('.wrap-map').on({
			'click' : function(){
				$(this).addClass('on')
			},
			'mouseleave' : function(){
				$(this).removeClass('on')
			}
		});
		fn.locationMap.render();
		fn.resize = function(){
			fn.rtime = new Date();
			if (fn.timeout === false) {
				fn.timeout = true;
				setTimeout(fn.locationMapReload, fn.delta);
			}
			if (co.respond){
				slider.item["program"].destroySlider();
				slider.item["program"].bxSlider(slider.programOption());
				slider.photoSet();
			}
		};
	};
};
</script>
<?php include_once "./inc/footer.php"; ?>
</body>
</html>