<?
CLASS Course extends Dbconn
{
	private $DB;
	private $GP;
	function __construct($DB = array()) {
		global $C_DB, $GP;
		$this -> DB = (!empty($DB))? $DB : $C_DB;
		$this -> GP = $GP;
	}
	
	// param
	function Course_Info($args) {
		if (is_array($args)) foreach ($args as $k => $v) ${$k} = $v;
		
		$qry = "
			select * from tblCourse where co_idx = '$co_idx'
		";
		$rst =  $this -> DB -> execSqlOneRow($qry);
		return $rst;
	}
	
	// param
	function Course_Satis_Info($co_idx) {
		$qry = "
			SELECT A.*,(SELECT co_satis FROM `tblCourse` WHERE A.co_mem_id = co_mem_id and co_end_date < A.co_end_date order by co_end_date desc ,co_idx desc limit 0,1) as before_satis 
				FROM `tblCourse` A where A.co_idx = '$co_idx';
		";
		$rst =  $this -> DB -> execSqlOneRow($qry);
		return $rst;
	}
	
	function Course_Update_Satis($args) {
		if (is_array($args)) foreach ($args as $k => $v) ${$k} = $v;
		
		$qry = "
			update tblCourse set co_satis = '$co_satis',co_improve = '$co_improve' where co_idx = '$co_idx'
		";
		$rst =  $this -> DB -> execSqlUpdate($qry);
		return $rst;
	}	

	// param
	function Course_List ($args = '') {
		global $C_Func;
		if (is_array($args)) foreach ($args as $k => $v) ${$k} = $v;

		global $C_ListClass;

		$tail = "";
		
		$addQry = " 1=1 ";

		if($search_f != "") $addQry .= " and co_field='".$search_f."' ";
		if($search_satis1 != "") $addQry .= " and co_satis>='".$search_satis1."' ";
		if($search_satis2 != "") $addQry .= " and co_satis<='".$search_satis2."' ";
		if($search_improve1 != "") $addQry .= " and co_improve>='".$search_improve1."' ";
		if($search_improve2 != "") $addQry .= " and co_improve<='".$search_improve2."' ";

		//exit;


		//$order = "co_improve desc,co_satis desc,co_reg_date desc";
		if($order == ""){
			$order = "co_satis desc";
		}

		if (($s_date && $e_date) && ($s_date < $e_date)) {
			if ($addQry)
			$addQry .= " AND ";

			if($coursetype == "satis"){
				$addQry .= " co_start_date BETWEEN '$s_date 00:00:00' AND '$e_date 00:00:00'";
			}
			else{
				$addQry .= " co_reg_date BETWEEN '$s_date 00:00:00' AND '$e_date 00:00:00'";
			}
		}
		if($co_mem_id) $addQry .= " AND co_mem_id = '$co_mem_id'";
		
		if ($search_key && $search_content) {
			if (!empty($addQry)) {
				$addQry .= " AND ";
				if($search_key == "all") {
					$addQry .= " (co_title LIKE ('%$search_content%') || co_contents LIKE ('%$search_content%'))";					
				}else{
				
					$addQry .= " $search_key LIKE ('%$search_content%')";
				}
			}
			$order = "co_satis desc";
		}
		$args['show_row'] = $show_row;
		$args['show_page'] = 5;
		$args['q_idx'] = "co_idx";
		$args['q_col'] = "*";
		$args['q_table'] = "tblCourse";
		$args['q_where'] = $addQry;
		$args['q_order'] = $order;
		$args['q_group'] = "";
		$args['tail'] = "search_f=".$search_f."&search_satis1=".$search_satis1."&search_satis2=".$search_satis2."&search_improve1=".$search_improve1."&search_improve2=".$search_improve2."&s_date=" . $s_date . "&e_date=" . $e_date ."&serach_key=" . $search_key . "&search_content=" . $search_cotent;
		$args['q_see'] = "";
		return $C_ListClass -> listInfo($args);
	}
	// param 
	function Course_Info_Modify($args) {
		if (is_array($args)) foreach ($args as $k => $v) ${$k} = $v;
		
		$qry = "
			Update
				tblCourse
			set			
				co_field 		= '$co_field',
				co_title 		= '$co_title',
				co_mem_id 		= '$co_mem_id',
				co_mem_name 	= '$co_mem_name',
				co_agency 		= '$co_agency',
				co_area 		= '$co_area',
				co_target 		= '$co_target',
				co_place 		= '$co_place',
				co_start_date 	= '$co_start_date',
				co_end_date 	= '$co_end_date',
				co_contents 	= '$co_contents',
				co_mod_date 	=  NOW()
			where
				co_idx='$co_idx'
		";
		$rst =  $this -> DB -> execSqlUpdate($qry);
		return $rst;
	}

	// param 
	function Course_Reg($args) {
		if (is_array($args)) foreach ($args as $k => $v) ${$k} = $v;
		
		$qry = "
			INSERT INTO
				tblCourse
				(
					co_idx,
					co_field,
					co_title,
					co_mem_id,
					co_mem_name,
					co_agency,
					co_area,
					co_target,
					co_place,
					co_start_date,
					co_end_date,
					co_contents,
					co_reg_date
				)
				VALUES
				(
					''
					, '$co_field'
					, '$co_title'
					, '$co_mem_id'
					, '$co_mem_name'
					, '$co_agency'
					, '$co_area'
					, '$co_target'
					, '$co_place'
					, '$co_start_date'
					, '$co_end_date'
					, '$co_contents'
					,  NOW()
				)
			";
		$rst =  $this -> DB -> execSqlInsert($qry);
		return $rst;
	}

	function Course_Update_Comment($args) {
		if (is_array($args)) foreach ($args as $k => $v) ${$k} = $v;
		
		$qry = "SELECT MAX(cc_step) AS max_step FROM tblCourseComment WHERE cc_co_idx='$cc_co_idx'";
		$rtn =  $this -> DB -> execSqlOneRow($qry);
		
		$cc_step = ($rtn['max_step'] > 0) ? $rtn['max_step'] + 1 : 1;
		
		$qry = "
			INSERT INTO
				tblCourseComment
				(
					cc_idx,				
					cc_co_idx,
					cc_mem_id,
					cc_mem_name,
					cc_mem_level,					
					cc_step,
					cc_depth,
					cc_reg_date,
					cc_contents,
					cc_del_flag
				)
				VALUES
				(
					''
					, '$cc_co_idx'
					, '$cc_mem_id'
					, '$cc_mem_name'
					, '$cc_mem_level'					
					, '$cc_step'
					, '1'
					,  NOW()
					,  '$cc_comments'
					, 'N'
				)
			";
		$rst =  $this -> DB -> execSqlInsert($qry);		
		return $rst;
	}
	
	function Course_Comment_List($co_idx) {
		if (is_array($args)) foreach ($args as $k => $v) ${$k} = $v;
		
		$qry = "
			select 
				* 
			from 
				tblCourseComment 
			where 
				cc_co_idx='$co_idx' and cc_del_flag='N' 
			order by cc_step asc
		";
		$rst =  $this -> DB -> execSqlList($qry);
		return $rst;
	}
	function Course_Comment_Order($co_idx) {
		if (is_array($args)) foreach ($args as $k => $v) ${$k} = $v;
		
		$qry = "
			SELECT * FROM `tblCourseComment` WHERE cc_co_idx='$co_idx' order by cc_step desc limit 0,1
		";
		$rst =  $this -> DB -> execSqlOneRow($qry);
		return $rst;
	}
	//강의일정 더보기
	function Course_New_List ($args = '') {
		global $C_Func;
		if (is_array($args)) foreach ($args as $k => $v) ${$k} = $v;

	 $db_config_data = $this -> Course_Info($args);

		if($cp_page) {
			$l_max = 5+(($cp_page - 1)*9);
			$limit = " limit $l_max,9";
		}else{
			$limit = " limit 0,9";
		}
	
		if($orderby != '') {
			$orderby  = " $orderby ";
		}else{
			$orderby  = " order by co_reg_date desc";
		}		
		
		$add_query = "";
		$add_query .= "co_mem_id = '$co_mem_id'"; 

		$qry = "
				SELECT * FROM tblCourse 
				WHERE
				 $add_query $orderby $limit
				";
		$rst =  $this -> DB -> execSqlList($qry);
		$qry = "select count(*) as cnt from tblCourse where $add_query";
		$t_cnt = $this -> DB -> execSqlOneRow($qry);
		$rst["t_cnt"] = $t_cnt["cnt"];
		return $rst;
	}	

	function Course_New_Reply($args = '') {
		global $C_Func;
		if (is_array($args)) foreach ($args as $k => $v) ${$k} = $v;

		global $C_ListClass;

		$tail = "";
		
		$addQry = " f.new_idx != '' and f.new_idx < 9";

		$order = "f.co_reg_date desc";

		if (($s_date && $e_date) && ($s_date < $e_date)) {
			if ($addQry)
			$addQry .= " AND ";

			$addQry .= " f.co_reg_date BETWEEN '$s_date 00:00:00' AND '$e_date 00:00:00'";
		}
		if($co_mem_id) $addQry .= " AND f.co_mem_id = '$co_mem_id'";
		
		if ($search_key && $search_content) {
			if (!empty($addQry)) {
				$addQry .= " AND ";
				if($search_key == "all") {
					$addQry .= " (f.co_title LIKE ('%$search_content%') || f.co_contents LIKE ('%$search_content%'))";					
				}else{
				
					$addQry .= " $search_key LIKE ('%$search_content%')";
				}
			}
			$order = "f.co_satis desc";
		}
		$args['show_row'] = $show_row;
		$args['show_page'] = 5;
		$args['q_idx'] = "f.co_idx";
		$args['q_col'] = "*";
		$args['q_table'] = "(SELECT *,(select cc_mem_level from tblCourseComment where cc_co_idx = A.co_idx order by cc_step desc limit 0,1) as new_idx  
				FROM tblCourse A WHERE 1=1 ORDER BY co_reg_date desc) f";
		$args['q_where'] = $addQry;
		$args['q_order'] = $order;
		$args['q_group'] = "";
		$args['tail'] = "s_date=" . $s_date . "&e_date=" . $e_date ."&serach_key=" . $search_key . "&search_content=" . $search_cotent;
		$args['q_see'] = "";
		return $C_ListClass -> listInfo($args);		
	}
	function Course_New_Reply_Cnt() {
		if (is_array($args)) foreach ($args as $k => $v) ${$k} = $v;
		
		$qry = "
				SELECT count(*) as cnt FROM 
					(SELECT *,(select cc_mem_level from tblCourseComment where cc_co_idx = A.co_idx order by cc_step desc limit 0,1) as new_idx FROM tblCourse A 
					WHERE 1=1 ORDER BY co_reg_date desc) f WHERE f.new_idx != '' and f.new_idx < 9 ORDER BY f.co_reg_date desc
		";
		$rst =  $this -> DB -> execSqlOneRow($qry);
		return $rst;
	}	

	function Course_Info_Del($args) {
		
		if (is_array($args)) foreach ($args as $k => $v) ${$k} = $v;
		
		$qry = "
			delete from tblCourse where co_idx = '$co_idx'
		";
		$rst =  $this -> DB -> execSqlUpdate($qry);
		return $rst;
	}



	function Course_count($co_mem_id){
		$qry = "select count(*) as cnt from tblCourse where co_mem_id='".$co_mem_id."'";
		$t_cnt = $this -> DB -> execSqlOneRow($qry);

		return $t_cnt["cnt"];
	}

	function Course_satis_cate(){
		$cate = [];

		$qry = '
			select co_title from tblCourse group by co_title;
		';
		$rst =  $this -> DB -> execSqlAssoc($qry);

		foreach($rst as $obj){
			$cate[] = $obj["co_title"];
		}

		return $cate;
	}

	function Course_field(){
		$cate = [];

		$qry = '
			select * from tblCourseField;
		';
		$rst =  $this -> DB -> execSqlOneRow($qry);
		$cf_name = $rst["cf_name"];
		
		$field = explode(",", $cf_name);

		return $field;
	}

	function Course_satis(){
		$where = "";

		if($_GET["search_key"] != ""){
			$where .= " and co_title='".$_GET["search_key"]."'";
		}

		if($_GET["uid"] != ""){
			$where .= " and co_mem_id='".$_GET["uid"]."'";
		}

		$qry = '
			select DATE_FORMAT(co_end_date, "%m") as month, TRUNCATE(avg(co_satis),1) as satis
			from tblCourse where DATE_FORMAT(co_end_date, "%Y")=\'2018\''.$where.'
			group by DATE_FORMAT(co_end_date, "%Y"), DATE_FORMAT(co_end_date, "%m");
		';
		$rst =  $this -> DB -> execSqlAssoc($qry);
		$json = [];
	
		if($rst){
			foreach($rst as $key=>$value){
				if(is_null($value['satis'])) $value['satis'] = "0.0";
				$json[intval($value['month'])] = $value['satis'];
			}
		}

		for($i=1; $i<=12; $i++){
			if(!array_key_exists($i, $json)){
				$json [$i] = "0.0";
			}
		}
		ksort($json);
		
		return @json_encode(array_values($json));
	}
}
?>