<?
// HEADER
@header("Content-Type: text/html; charset=utf-8");
@header("Cache-Control: no-cache, must-revalidate");
@header("Pragma: no-cache");
@header('P3P: CP="NOI CURa ADMa DEVa TAIa OUR DELa BUS IND PHY ONL UNI COM NAV INT DEM PRE"');
@Header("Access-Control-Allow-Origin: *");
@Header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
@header("Access-Control-Allow-Headers: X-Requested-With, X-Prototype-Version");
@session_start();

@ini_set("session.cache_expire",3600*2);
@ini_set("session.gc_maxlifetime",3600*2);

@extract($_GET); 
@extract($_POST); 
@extract($_SERVER);
//error_reporting(E_ALL^E_NOTICE);
//@ini_set("display_errors", 1);


class object {};
$GP = new object;

////////////////////////////////////////////////////////////////
//# Path config
////////////////////////////////////////////////////////////////
// default include path
$GP -> DOCROOT	= $_DEF_PATH;
$GP -> WEBROOT 		= '/';
$GP -> ROOT 		= $GP -> DOCROOT . $GP -> WEBROOT;

$GP -> CLS		= $GP -> ROOT . '_CLASS/';
$GP -> INC		= $GP -> ROOT . '_INC/';
$GP -> COM		= $GP -> ROOT . '_COMMON/';
$GP -> HOME		= $GP -> ROOT . 'public_html/';

$GP -> IP								= $_SERVER['SERVER_ADDR'];
$GP -> DOMAIN						= $_SERVER['HTTP_HOST'];
$GP -> SERVICE_DOMAIN		= 'http://hellokac.me';
$GP -> HTTPS						= 'https://' . $GP -> DOMAIN;
$GP -> HTTP							= 'http://' . $GP -> DOMAIN;
$GP -> HTTPS_PORT				= 443;

$GP -> QUERY_STRING = $_SERVER['QUERY_STRING'];
$GP -> SELFPAGE 		= $_SERVER['PHP_SELF'];
$GP -> NOWPAGE 			= $GP -> SELFPAGE . '?' . $_SERVER['QUERY_STRING'];
$GP -> NOWPAGE_ENC 	= urlencode($GP -> NOWPAGE);

$GP -> INC_ADM = $GP -> HOME ."admin/";
$GP -> INC_ADM_PATH = $GP -> HOME ."admin/inc/";
$GP -> INC_ADM_PATH_NEW = $GP -> HOME ."admin/inc/";

$GP -> INC_WWW	= $GP -> HOME ."inc";
$GP -> INC_PATH = $GP -> HOME . "bbs";
$GP -> IMG_PATH = $GP -> HTTP . "/bbs";
$GP -> JS_SMART_PATH = $GP -> HTTP . "/bbs/smarteditor/js";
$GP -> JS_PATH = $GP -> HTTP . "/js";

## 스마트에디터 업로드 경로
$GP -> UP_IMG_SMARTEDITOR		= $GP -> HOME . 'bbs/files/';
$GP -> UP_IMG_SMARTEDITOR_URL	= $GP -> HTTP . '/bbs/files/';



## 팝업 링크
$GP -> UP_POPUP			= $GP -> HOME . 'popup/upfile/';
$GP -> UP_POPUP_URL		= $GP -> HTTP . '/popup/upfile/';

## 슬라이드 관련
$GP -> UP_SLIDE			= $GP -> HOME . 'common/slide/';
$GP -> UP_SLIDE_URL		= $GP -> HTTP . '/common/slide/';



## 의료진 관련
$GP -> UP_COURSE			= $GP -> HOME . 'common/course/';
$GP -> UP_COURSE_URL		= $GP -> HTTP . '/common/course/';




////////////////////////////////////////////////////////////////
//# Load class
////////////////////////////////////////////////////////////////
// 기본 함수
include_once $GP -> CLS . 'class.func.php';
$C_Func 		= new Func;

////////////////////////////////////////////////////////////////
//# Database config
////////////////////////////////////////////////////////////////
include_once $GP -> INC . 'db.inc';


////////////////////////////////////////////////////////////////
//# Admin config
////////////////////////////////////////////////////////////////
$GP -> Admin_Email = "all@hellokac.me";
$GP -> Admin_Phone = "051-711-9500";
$GP -> Admin_Name = "관리자";
$GP -> Admin_HP_NAME = "KAC";
$GP -> Admin_HP_NAME1 = "KAC";
$GP -> Admin_BizNum = "";
$GP -> SMS_HP1 = "051";
$GP -> SMS_HP2 = "711";
$GP -> SMS_HP3 = "9500";

$GP -> HOME_MAIN_ADDRESS = "부산시 동구 범일로 85";


////////////////////////////////////////////////////////////////
//# sms config
////////////////////////////////////////////////////////////////
$GP -> SMS_ID = '';
$GP -> SMS_AUTH_KEY ='';

$GP->DB_TABLE = "hellokac";

////////////////////////////////////////////////////////////////
//# SMTP config
////////////////////////////////////////////////////////////////
$GP -> SMTP_USE 		= true;
$GP -> SMTP_SERVER	= 'uws64-124.cafe24.com';
$GP -> SMTP_IP		= '183.111.161.106';
$GP -> SMTP_PORT		= '587';
$GP -> SMTP_USER		= 'all@hellokac.me';
$GP -> SMTP_PASS		= 'kachello!@';

////////////////////////////////////////////////////////////////
//# Pass config
////////////////////////////////////////////////////////////////
$GP -> PASS = 'ka-100';


////////////////////////////////////////////////////////////////
//# 권한 설정
////////////////////////////////////////////////////////////////
$GP -> AUTH_LEVEL	 = array(
						"9" => "관리자"
						, "5" => "부관리자"
					);

$GP -> BOARD_CONFIG_LEVEL	 = array(
						"0" => "일반"
						, "3" => "회원"
						, "9" => "관리자"
					);

$GP -> MEM_SEX	 = array(
						"M" => "남"
						, "F" => "여"
					);

$GP -> UNION_TYPE	 = array(
						"A" => "조합원"
						, "B" => "비조합원"
					);

$GP -> QNA_RESULT	 = array(
						"N" => "미처리"
						, "M" => "처리중"
						, "Y" => "처리완료"
					);

$GP -> QNA_USER_TYPE	 = array(
						"M" => "회원"
						, "G" => "비회원"
					);
//요일 정보
$GP -> GL_WEEK_INFO = array (
    					"sun" => "일",
                        "mon" => "월",
                        "tue" => "화",
                        "wed" => "수",
                        "thu" => "목",
                        "fri" => "금",
                        "set" => "토"
                    );

//전화번호 기본 입력(휴대폰만)
$GP -> MOBILE =
        				array(
        					"010" => "010"
        					, "011" => "011"
        					, "016" => "016"
        					, "017" => "017"
        					, "018" => "018"
        					, "019" => "019"
        				);

//전화번호 기본 입력(휴대폰포함)
$GP -> TELEPHONE =
        				array(
        					"02" => "02"
        					, "031" => "031"
        					, "032" => "032"
        					, "033" => "033"
        					, "041" => "041"
        					, "042" => "042"
        					, "043" => "043"
        					, "051" => "051"
        					, "052" => "052"
        					, "053" => "053"
        					, "054" => "054"
        					, "055" => "055"
        					, "061" => "061"
        					, "062" => "062"
        					, "063" => "063"
        					, "064" => "064"
        					, "070" => "070"
        					, "080" => "080"
        					, "0505" => "0505"
        					, "1544" => "1544"
        					, "1566" => "1566"
        					, "1577" => "1577"
        					, "1588" => "1588"
        					, "1599" => "1599"
        					, "1577" => "1577"
        				);


/************************************************/
/*          이메일 정보                         */
/************************************************/
$GP -> EMAIL =
				array (
						'hanmail.net' => '다음(한메일)'
						, 'yahoo.com' => '야후'
						, 'naver.com' => '네이버'
						, 'paran.com' => '파란'
						, 'nate.com' => '네이트'
						, 'empal.com' => '엠팔'
						, 'hitel.net' => '하이텔'
						, 'hanmir.com' => '한미르'
						, 'chol.com' => '천리안'
						, 'korea.com' => '코리아닷컴'
						, 'netian.com' => '네띠앙'
						, 'dreamwiz.com' => '드림위즈'
						, 'lycos.co.kr' => '라이코스'
						, 'orgio.net' => '오르지오'
						, 'unitel.co.kr' => '유니텔'
						, 'kornet.net' => '코넷'
						, 'freechal.com' => '프리첼'
						, 'hanafos.com' => '하나포스'
						, 'hotmail.com' => '핫메일'
						, 'gmail.com' => '구글'
						, 'msn.com' => 'MSN'
				);




$GP-> WEB_TITLE = "KAC";
$GP-> ADM_TITLE = "KAC 관리자 페이지";
?>
